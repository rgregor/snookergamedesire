'use strict';

angular.module('myApp.services', []).
value('version', '0.1').
// Fetches the content of a given url. Shows a loading message designated by
// parameter msg. Uses a byDayIndexCache internally to make the HTTP call only once.
factory('Load', function($http, $window, $q) {
	var isLoading = false;
	var loadingMsgs = [];
	var byDayIndexCache = {};

	function onRequestFinished() {
		if(loadingMsgs.length < 1) {
			$window.$.unblockUI();
			isLoading = false;
		}
		else {
			$window.$.unblockUI();
			blockUI(loadingMsgs.pop());
		}
	}

	function blockUI(msg) {
		$window.$.blockUI({css : {border : "none"}, message : "<div class='alert alert-info alert-block'><h3>" + msg + "</h3></div>"}); 
	}

	return function(url, msg) {
		var deferred = $q.defer();

		if(byDayIndexCache[url]) {
			deferred.resolve(byDayIndexCache[url]);
			return deferred.promise; 
		}
		if(! isLoading) {
			blockUI(msg);
			isLoading = true;
		}
		else {
			loadingMsgs.unshift(msg);
		}
		$http.get(url).success(function(data) {
			byDayIndexCache[url] = data;
			deferred.resolve(data);	
			onRequestFinished();
		}).error(function(data, status, headers, config) {
			onRequestFinished();
			deferred.reject("Status " + status + ". Failed to load the following resource: " + url + ".");
		});
		return deferred.promise;
	};
}).
// Some helper functions used by controllers
factory('Helpers', function($rootScope, $location) {
	var myHelpers = {
		handleUserChange : function(users) {
			var userParam = $location.search().user;
			if(!_.contains(users, userParam)) {
				var newUser = $rootScope.selectedUser ? $rootScope.selectedUser : users[0];
				$location.search("user", newUser);
			}
			else newUser = userParam;
			$rootScope.selectedUser = newUser;
		},
roundDateToDayString : function(date) {
	date = Date.fromString(date);
	return myHelpers.padNumber(date.getDate()) + "." + (myHelpers.padNumber(date.getMonth()+1)) + "." + (date.getYear()+1900);
},

getNumberOfProperties : function(obj) { 
	var propNames = Object.getOwnPropertyNames(obj);
	return propNames.length;
},

	padNumber : function(nr) {
		if(nr<10) return "0" + nr;
		else return nr;
	}
};
return myHelpers;


}).
factory('MatchesHelper', function(Helpers) {
	var byDayIndexCache = undefined;
	var matchesOfPlayerCache = {};
	var avgMaxBreakCache = {};
	var maxBreaksCache = {};
	var maxBreakPercentagePerLevelCache = {};
	var matchesWithUsersCache = {};
	var resultsCache = {};
	var opponentsCache = {};
	var rankingCache = {};

	var matchHelpers = {
		getRankingInfo : function(matches, player) {
			var cache = rankingCache[player];
			if(cache) return cache;
			var myMatches = _.filter(matchHelpers.getMatchesOfPlayer(matches, player), function(match) {
				return match.ranking1 != 0; 
			});
			var info = {};
			var myTitle = "Ranking of " + player;
			info.options = { title : myTitle, legend : {position : "none" } };
			info.data = {};
			info.data.cols = [{id : '', label : 'Date', type : 'date'}, {id : '', label : 'Ranking', type : 'number'}];
			var rows = [];
			var minRating = 8000;
			var maxRating = 0;
			var ranking;
			_.each(myMatches, function(match) {
				ranking = (match.player1 === player) ? match.ranking1 : match.ranking2;
				if(ranking > maxRating) maxRating = ranking;
				if(ranking < minRating) minRating = ranking;
				var rowObj = {};
				rowObj.c = [];
				rowObj.c.push({v : Date.fromString(match.date)});
				rowObj.c.push({v : ranking});
				rows.push(rowObj);
			});
			info.min = minRating;
			info.max = maxRating;
			info.curr = ranking;
			info.data.rows = rows;

			rankingCache[player] = info;
			return info; 
		},

		getBreaks : function(matches, player, last100) {
			var cachedBreaks = maxBreaksCache[player+last100];
			if(cachedBreaks) return cachedBreaks;
			var breaks =_.filter(_.map(matches, function(match) {
				if(match.player1===player) return match.maxBreak1;
				if(match.player2===player) return match.maxBreak2;
				return 0; 
			}), function(maxBreak) { return maxBreak !== 0; });
			if(last100) {
				breaks = breaks.slice(-100);
			}
			maxBreaksCache[player+last100] = breaks;
			return breaks;

		},

		getMatchesWithUsers : function(matches, user1, user2) {
			var cache = matchesWithUsersCache[user1+user2];
			if(cache) return cache;
			var matchesWanted = [];
			var myMatches = matchHelpers.getMatchesOfPlayer(matches, user1);
			_.each(myMatches, function(match) {
				var opponent = (match.player1 == user1) ? match.player2 : match.player1;
				if(opponent == user2) matchesWanted.push(match);
			});
			matchesWithUsersCache[user1+user2] = matchesWanted;
			matchesWithUsersCache[user2+user1] = matchesWanted;
			return matchesWanted;
		},

		getResults : function(allMatches, user1, user2) {
			var cache = resultsCache[user1+user2];
			if(cache) return cache;
			var matches = matchHelpers.getMatchesWithUsers(allMatches, user1, user2);
			var result = {};
			result[user1] = {};
			result[user2] = {};
			result[user1].total = 0;
			result[user1].difference = 0;
			result[user2].total = 0;
			result[user2].difference = 0;

			_.each(matches, function(match) {
				var winner = (match.winner === user1) ? user1 : user2;
				result[winner].total += 1;
				var diffUser1 = (match.player1 === user1) ? match.difference1 : match.difference2;
				var diffUser2 = (match.player1 === user2) ? match.difference1 : match.difference2;
				result[user1].difference += diffUser1;
				result[user2].difference += diffUser2;
			});
			result[user1].percent = result[user1].total / matches.length;
			result[user2].percent = result[user2].total / matches.length;
			result[user1].difference = Math.round(result[user1].difference*100)/100;
			result[user2].difference = Math.round(result[user2].difference*100)/100;

			resultsCache[user1+user2] = result;
			resultsCache[user2+user1] = result;
			return result;
		},

		getOpponents : function(matches, user) {
			var cache = opponentsCache[user];
			if(cache) return cache;
			var opponents = [];
			var myMatches = matchHelpers.getMatchesOfPlayer(matches, user);
			_.each(myMatches, function(match) {
				var opponent = (match.player1===user) ? match.player2 : match.player1;
				if(!_.contains(opponents, opponent)) opponents.push(opponent);
			});
			opponents.sort(function (a, b) {
				    return a.toLowerCase().localeCompare(b.toLowerCase());
			});
			opponentsCache[user] = opponents;
			return opponents;
		},

		// Filters the matches in which the player took part
		getMatchesOfPlayer : function(matches, player) {
			if(matchesOfPlayerCache[player]) return matchesOfPlayerCache[player];

			var filteredMatches =  _.filter(matches, function(match) {
				return match.player1===player|| match.player2===player;

			});
			matchesOfPlayerCache[player] = filteredMatches;
			return filteredMatches;	
		},

		getTopBreaks : function(matches, player, count) {
			var breaks = matchHelpers.getBreaks(matches, player);
			var breakCounts = {};
			var topBreakCounts = {};
			for (var i=0; i<breaks.length; i++) {
				var num = breaks[i];
				breakCounts[num] = breakCounts[num] ? breakCounts[num]+1 : 1; 
			}
			var uniqBreaks = _.uniq(breaks).sort(function(a,b) {return a-b;}).reverse().splice(0,count);

			for (var i=0; i<uniqBreaks.length; i++) {
				topBreakCounts[i] = {};
				topBreakCounts[i].topBreak = uniqBreaks[i] ;
				topBreakCounts[i].times = breakCounts[uniqBreaks[i]] ;
			}

			return topBreakCounts;
		},
		getAvgMaxBreak : function(matches, player, last100) {
			if(avgMaxBreakCache[player+last100]) return avgMaxBreakCache[player+last100];
			var breaks = matchHelpers.getBreaks(matches, player, last100); 
			var avgBreak = _.reduce(breaks, function(a,b) { return a+b; }, 0) / breaks.length;
			avgBreak = Math.round(avgBreak * 100) / 100;
			avgMaxBreakCache[player+last100] =  avgBreak;
			return avgBreak;

		},

		getMaxBreakPercentagePerLevel : function(matches, player, levels, last100) {
			if(maxBreakPercentagePerLevelCache[player+last100]) return maxBreakPercentagePerLevelCache[player+last100];
			var breaks = matchHelpers.getBreaks(matches, player, last100); 

			// Initialize breaks counter
			var breaksPerLevelCounter = {};
			for(var i=0; i<=levels.length; i++) {
				breaksPerLevelCounter[levels[i]] = 0;
			}


			for (var i=0; i<=breaks.length; i++) {
				for(var o=0; o<=levels.length; o++) {
					var level = levels[o];
					if(breaks[i]>=level) breaksPerLevelCounter[level] = breaksPerLevelCounter[level] + 1;
				}	
			}

		  var breaksPerLevel = {};
			for(var i=0; i<=levels.length; i++) {
				var level = levels[i];
				breaksPerLevel[level] = {};
			  breaksPerLevel[level].percentage	= breaksPerLevelCounter[level] / breaks.length;
				breaksPerLevel[level].total = breaksPerLevelCounter[level];
			}

			maxBreakPercentagePerLevelCache[player+last100] = breaksPerLevel;
			return breaksPerLevel;

		},

		// Helps to get the matches of the three last days for the "Latest" page
		// Returns an array with the most current matches in index 1, the next most current in 2 and the third most current matches in index 3
		getMatchesByDayIndex : function(matches) { 
			if(byDayIndexCache) return byDayIndexCache;

			var matchesByDay = {};
			for (var i=0; i<=matches.length; i++) {
				var match = matches[i];
				var matchDate = Helpers.roundDateToDayString(match.date);	
				var matchesOfDay = matchesByDay[matchDate];
				if(matchesOfDay) {
					matchesOfDay.push(match);
				}
				else {
					matchesByDay[matchDate] = [match];
					if(Helpers.getNumberOfProperties(matchesByDay)>=4) break;
				}
			}

			var dates = [];
			for (var prop in matchesByDay) {
				if(matchesByDay.hasOwnProperty(prop)) {
					dates.push(prop);
				}
			}
			dates.sort(function (date1String, date2String) {
				var date1 = Date.fromString(date1String, {order : 'DMY'});
				var date2 = Date.fromString(date2String, {order : 'DMY'});
				if (date1 > date2) { 
					return -1;
				}
				if (date1 < date2) {
					return 1;
				}
				return 0;
			});
			var matchesByDayIndex = { 1 : matchesByDay[dates[0]], 2 : matchesByDay[dates[1]], 3 : matchesByDay[dates[2]] };  
			byDayIndexCache = matchesByDayIndex;

			return matchesByDayIndex;

		}
	};
	return matchHelpers;
}).
factory('MatchDisplay', function() {
	return {
		getLoser : function(match) {
			var winner = match.winner;
			return (winner == match.player1) ? match.player2 : match.player1;
		},

getBreakWinner : function(match) {
	var winner = match.winner;
	return (winner == match.player1) ? match.maxBreak1 : match.maxBreak2;
},

getBreakLoser : function(match) {
	var winner = match.winner;
	return (winner == match.player1) ? match.maxBreak2 : match.maxBreak1;
}
}
});
