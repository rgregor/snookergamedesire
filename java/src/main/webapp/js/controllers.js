'use strict';

/* Controllers */
google.load("visualization", "1", {packages:["corechart"]});

// Loads the necessary matches and users data. Every page controller
// makes use of this resolving process in the routes declaration
RootCtl.resolve = {
	users : function(Load, $q, $rootScope) {
		var deferred = $q.defer();
		var usersLoaded = Load("favorites.txt", "Loading users...");
		usersLoaded.then(function(usersString) {
			var users = usersString.split(',');
			deferred.resolve(users);
		}, function(failReason) { 
			$rootScope.$broadcast('errorLoading', failReason); 
		});
		return deferred.promise;
	},
	matches : function(Load, $window, $q, $rootScope) {
		var deferred = $q.defer();
		var matchesLoaded = Load("matches", "Loading matches...");
		matchesLoaded.then(function(matches) {
			deferred.resolve(matches);
		}, function(failReason) {
			$rootScope.$broadcast('errorLoading', failReason); 
		});
		return deferred.promise;
	}
};

function RootCtl($scope, $rootScope, $location) {
	// error message for failed loading of resources
	$scope.error = "";	

	// for easy usage in the template
	$scope.root = $rootScope;

	// react to the errorLoading event which designates the failure to load a necessary resource
	$scope.$on('errorLoading', function(event, failReason) {
		$scope.error = failReason + " Please refresh the page or contact the administrator!";
	});

	// react to a change of the selected user
	$rootScope.$watch("selectedUser", function(newUser, oldUser) {
		if(newUser!=oldUser) {
			$location.search("user", newUser);
		}
	});
}

function MatchesCtl($scope, $rootScope, $route, $location, MatchesHelper, Helpers, MatchDisplay, matches, users) {
	Helpers.handleUserChange(users);

	// set activePage for highlighting current page in navigation
	$rootScope.activePage = $route.current.activePage;

	// for easy usage in the template
	$scope.root = $rootScope;
	$scope.MatchDisplay = MatchDisplay;
	$scope.Helpers = Helpers;

	$scope.users = users;

	// get the matches of the selected player
	$scope.allMatches = MatchesHelper.getMatchesOfPlayer(matches.slice(), $rootScope.selectedUser).slice().reverse();

	var perPage = 20;
	var numberOfPagesToShow = 4;
	var pagesNr = Math.ceil($scope.allMatches.length / perPage);
	$scope.activePage = 1;
	$scope.pagesToShow = [];

	$scope.showPage = function(page) {
		if(page<1||page>pagesNr) return;
		$scope.activePage = page;
		$scope.disableNext = (page === pagesNr);
		$scope.disablePrevious = (page === 1);

		var otherPagesLeftBorder = page - numberOfPagesToShow;
		var otherPagesRightBorder = page + numberOfPagesToShow+1;
		var leftBorderDiff = 1-otherPagesLeftBorder; //if positive => omit leftBorderDiff times page links on the left
		var rightBorderDiff = otherPagesRightBorder-pagesNr; //if positive => omit rightBorderDiff times page right on the left

		if(leftBorderDiff > 0) {
			if(rightBorderDiff < 0) {
				var possibleExtension = rightBorderDiff * -1;
				var extension = leftBorderDiff < possibleExtension ? leftBorderDiff : possibleExtension;
				otherPagesRightBorder = otherPagesRightBorder + extension;
			}
		}
		else {
			if(rightBorderDiff > 0) {
				var possibleExtension = leftBorderDiff * -1;
				var extension = rightBorderDiff < possibleExtension ? rightBorderDiff : possibleExtension;
				otherPagesLeftBorder = otherPagesLeftBorder - extension;
			}
		}

		var pages = [];
		for (var i=otherPagesLeftBorder; i<=otherPagesRightBorder; i++) {
			pages.push(i);
		}
		$scope.pagesToShow = _.filter(pages, function(num) { return num>=1&&num<=pagesNr; });
		$scope.matches = $scope.allMatches.slice().splice((page-1)*perPage, perPage);
	};
	$scope.showPage($scope.activePage);

	$scope.formatDate = function(date) {
		date = Date.fromString(date);
		return Helpers.padNumber(date.getDate()) + "." + Helpers.padNumber(date.getMonth()+1) + "." + (date.getYear()+1900) + "   " + Helpers.padNumber(date.getHours()) + ":" + Helpers.padNumber(date.getMinutes());
	};
};

function RankingCtl($scope, $rootScope, $route, Helpers, MatchesHelper, matches, users) {
	Helpers.handleUserChange(users);

	// set activePage for highlighting current page in navigation
	$rootScope.activePage = $route.current.activePage;
	$scope.users = users;

	var allMatches = matches.slice();
	var rankingInfo = MatchesHelper.getRankingInfo(allMatches, $rootScope.selectedUser);

	$scope.minRanking = rankingInfo.min;
	$scope.maxRanking = rankingInfo.max;
	$scope.currentRanking = rankingInfo.curr;


	var chart = new google.visualization.LineChart(document.getElementById('rankingContainer'));
	var dataTable = new google.visualization.DataTable(rankingInfo.data);

	chart.draw(dataTable, rankingInfo.options);
}

function BreaksCtl($scope, $rootScope, $route, $location, Helpers, MatchesHelper, matches, users) {
	Helpers.handleUserChange(users);

	// set activePage for highlighting current page in navigation
	$rootScope.activePage = $route.current.activePage;

	$scope.users = users;

	// get the matches of the selected player
	$scope.numberBreaks = MatchesHelper.getBreaks(matches.slice(), $rootScope.selectedUser, false).length;
	$scope.numberBreaksRecent = MatchesHelper.getBreaks(matches, $rootScope.selectedUser, true).length;

	$scope.avgMaxBreak = MatchesHelper.getAvgMaxBreak(matches, $rootScope.selectedUser, false);
	$scope.avgMaxBreakRecent = MatchesHelper.getAvgMaxBreak(matches, $rootScope.selectedUser, true);

	$scope.maxBreakPercentagePerLevel = MatchesHelper.getMaxBreakPercentagePerLevel(matches, $rootScope.selectedUser, [20, 30, 50, 75, 100], false);
	$scope.maxBreakPercentagePerLevel100 = MatchesHelper.getMaxBreakPercentagePerLevel(matches, $rootScope.selectedUser, [20, 30, 50, 75, 100], true);
	$scope.topMaxBreaks = MatchesHelper.getTopBreaks(matches, $rootScope.selectedUser, 20);
}

function HeadCtl($scope, $route, $rootScope, MatchesHelper, Helpers, matches, users) {
	// set activePage for highlighting current page in navigation
	$rootScope.activePage = $route.current.activePage;
	$scope.users = users;
	var allMatches = matches.slice();


	$scope.player1 = users[0];
	$scope.$watch("player1", function(newValue, oldValue)  {
		$scope.opponents = MatchesHelper.getOpponents(allMatches, newValue);
		$scope.player2 = $scope.opponents[0];

		$scope.results = MatchesHelper.getResults(allMatches, $scope.player1, $scope.player2);
		updateColumnClass();
	});

	$scope.$watch("player2", function(newValue, oldValue)  {
		$scope.results = MatchesHelper.getResults(allMatches, $scope.player1, $scope.player2);
		updateColumnClass();
	});

	function updateColumnClass() {
		var winDiff = $scope.results[$scope.player1].total - $scope.results[$scope.player2].total;
		$scope.classPlayer1 = (winDiff < 0) ? "loser" : (winDiff == 0) ? "info" : "winner";
		$scope.classPlayer2 = (winDiff < 0) ? "winner" : (winDiff == 0) ? "info" : "loser";
	}
}

function LatestMatchesCtl($scope, $rootScope, $route, Helpers, MatchesHelper, MatchDisplay, matches, users) {
	// set activePage for highlighting current page in navigation
	$rootScope.activePage = $route.current.activePage;

	// for easy usage in the template
	$scope.MatchDisplay = MatchDisplay;

	var matchesReversed = matches.slice().reverse();
	$scope.matchesByDayIndex = MatchesHelper.getMatchesByDayIndex(matchesReversed);

	$scope.dateToString = function(date) {
		return Helpers.roundDateToDayString(date);
	}

	$scope.getMatchesOfDay = function(dayNr) {
		return matchesByDayIndex[dayNr];
	};

	$scope.formatDate = function(date) {
		var myDate = Date.fromString(date);
		return Helpers.padNumber(myDate.getHours()) + ":" + Helpers.padNumber(myDate.getMinutes());
	};
}
