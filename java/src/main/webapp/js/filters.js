'use strict';

/* Filters */

angular.module('myApp.filters', []).
filter('interpolate', ['version', function(version) {
	return function(text) {
		return String(text).replace(/\%VERSION\%/mg, version);
	}
}]).filter('shortenName', function() {
	return function(name) { var myLength = 15;
		if(name==undefined) return "";
		if(name.length <= myLength) return name;
		else return name.substring(0, myLength);
	};}).filter('percentage', function() {
		return function(number) { 
			var percentage = number * 100;
			var percentageStr = percentage+'';
			if(percentageStr.length<5) return percentageStr + "%";
			else return percentageStr.substring(0,5) + "%";
		};});



