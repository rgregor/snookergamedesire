'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', ['myApp.filters', 'myApp.services', 'myApp.directives']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/latest', {templateUrl: 'partials/latest.html', controller: LatestMatchesCtl, resolve: RootCtl.resolve, activePage: 'latest'});
    $routeProvider.when('/ranking', {templateUrl: 'partials/ranking.html', controller: RankingCtl, resolve: RootCtl.resolve, activePage: 'ranking'});
    $routeProvider.when('/matches', {templateUrl: 'partials/matches.html', controller: MatchesCtl, resolve: RootCtl.resolve, activePage: 'matches'});
    $routeProvider.when('/breaks', {templateUrl: 'partials/breaks.html', controller: BreaksCtl, resolve: RootCtl.resolve, activePage: 'breaks'});
    $routeProvider.when('/headtohead', {templateUrl: 'partials/head.html', controller: HeadCtl, resolve: RootCtl.resolve, activePage: 'headtohead'});
    $routeProvider.otherwise({redirectTo: '/latest'});
  }]).run(function($rootScope) {
		//$rootScope.loadingMsgs = [];
	});
