package com.greg.gamedesire.server;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.List;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;

public interface IHTMLParser {

	public abstract List<Date> getMatchDates(String path)
			throws FailingHttpStatusCodeException, MalformedURLException,
			IOException, InterruptedException;

	public abstract String parse(String path) throws ServerException;

}
