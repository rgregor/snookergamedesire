package com.greg.gamedesire.server;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SimpleTimeZone;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.greg.gamedesire.server.persistence.IEntitiesContainerDao;
import com.greg.gamedesire.shared.dto.Match;

@Singleton
public class MatchesServlet extends HttpServlet {
	private static final long serialVersionUID = 5462156276132690017L;
	private static final Logger log = Logger.getLogger(MatchesServlet.class.getName());
	IEntitiesContainerDao matchesDao;
	IMatchesFilter matchesFilter;
	
	 /**
     * @see HttpServlet#HttpServlet()
     */
	@Inject
    public MatchesServlet(IEntitiesContainerDao matchesDao, IMatchesFilter matchesFilter) {
        super();
        this.matchesDao = matchesDao;
        this.matchesFilter = matchesFilter;
    }
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletOutputStream outputStream = response.getOutputStream();
		
		long beforeGetAll = System.currentTimeMillis();
		List<Match> allMatches = (List<Match>) matchesDao.getAll();
		
		long afterGetAll = System.currentTimeMillis();
		
		SimpleTimeZone mez = new SimpleTimeZone( +1*60*60*1000, "ECT" );
		mez.setStartRule( Calendar.MARCH, -1, Calendar.SUNDAY, 2*60*60*1000 );
		mez.setEndRule( Calendar.OCTOBER, -1, Calendar.SUNDAY, 2*60*60*1000 );
		
		List<Match> copyMatches = new ArrayList<Match>();
		for (Match match : allMatches) {
			Match newMatch = new Match(match);
			copyMatches.add(newMatch);
			newMatch.date = new Date(newMatch.date.getTime() + 1000 * 60 * 60);
			if(mez.inDaylightTime(newMatch.date)) {
				newMatch.date = new Date(newMatch.date.getTime() + 1000 * 60 * 60);
			}
		}
			
		Collections.sort(copyMatches);
		
		String nMatch = request.getParameter("nMatch");

		int dropMatches = 0;
		if(nMatch != null && !nMatch.isEmpty()) {
			dropMatches = Integer.valueOf(nMatch);
		}
		
		
		log.severe(String.format("matchesDAO.getAll took milliseconds: %d", (afterGetAll-beforeGetAll)));
		
		String json = toJson(copyMatches);
		
		long beforeMatchesFilter = System.currentTimeMillis();
		
		String[] favorites = ServerUtility.getFavorites(getServletContext()).toArray(new String[] { });
		json = matchesFilter.filterMatches(json, dropMatches, favorites);
		long afterMatchesFilter = System.currentTimeMillis();
		
		log.severe(String.format("Clojure filtering took: %d", (afterMatchesFilter-beforeMatchesFilter)));
		
		
		outputStream.print(json);
		outputStream.flush();
	}

	private String toJson(List<Match> copyMatches) {
		Gson gson = new Gson();
		Type listType = new TypeToken<List<Match>>() {}.getType();
		
		long beforetoJson = System.currentTimeMillis();		
		String json = gson.toJson(copyMatches, listType);
		long aftertoJson = System.currentTimeMillis();
		
		log.severe(String.format("gson.toJson took milliseconds: %d", (aftertoJson-beforetoJson)));
		return json;
	}

	

}
