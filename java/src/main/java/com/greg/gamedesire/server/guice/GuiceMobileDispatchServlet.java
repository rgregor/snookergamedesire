package com.greg.gamedesire.server.guice;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import net.customware.gwt.dispatch.server.Dispatch;
import net.customware.gwt.dispatch.server.guice.GuiceStandardDispatchServlet;

import com.google.gwt.user.server.rpc.SerializationPolicy;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Servlet answering rpc calls, extends standard guice dispatch servlet for being able 
 * to serve on the frontend as well as on the mobile app. 
 */
@Singleton
public class GuiceMobileDispatchServlet extends GuiceStandardDispatchServlet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5563760326632629138L;
	private static final Logger log = Logger.getLogger(GuiceMobileDispatchServlet.class.getName());


	@Inject
	public GuiceMobileDispatchServlet(Dispatch dispatch)
	{
		super(dispatch);
	}


	/**
	 * Provides correct base url of gwt files
	 * 
	 * @return base url for gwt files, fitting to being called from the mobile app as well as from the web frontend.
	 */
	private String getBaseUrl(HttpServletRequest request)
	{
		String baseUrl;
		if(request.getServerPort() == 8888 || request.getServerPort() == 80 || request.getServerPort() == 443)
		{
			baseUrl = request.getScheme() + "://" + "127.0.0.1" + request.getContextPath() + "/";
		}
		else
		{
			baseUrl = request.getScheme() + "://" + "127.0.0.1" + ":" + request.getServerPort() + request.getContextPath() + "/";
		}
		log.fine(String.format("BaseUrl is %s", baseUrl));
		return baseUrl;
	}


	@Override
	protected SerializationPolicy doGetSerializationPolicy(HttpServletRequest request, String moduleBaseURL, String strongName)
	{
		String baseUrl = getBaseUrl(request);
		return super.doGetSerializationPolicy(request, baseUrl, strongName);
	}

}
