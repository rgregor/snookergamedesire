package com.greg.gamedesire.server;

/**
 * Contains a value of type parameter A and 
 * a string.
 * 
 * @author Gregor Riegler
 */
public class ValueContainer<A> {
		public String remainingString;
		public A field;		
		
		public ValueContainer(A field, String string) {
			this.field = field;
			this.remainingString = string;
		}
	
}