package com.greg.gamedesire.server.guice;

import com.google.apphosting.utils.remoteapi.RemoteApiServlet;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.greg.gamedesire.server.IHTMLParser;
import com.greg.gamedesire.server.IMatchesExtractor;
import com.greg.gamedesire.server.IMatchesFilter;
import com.greg.gamedesire.server.ServerUtility;
import com.greg.gamedesire.server.impl.HTMLParserImpl;
import com.greg.gamedesire.server.impl.MatchesExtractorImpl;
import com.greg.gamedesire.server.impl.MatchesFilterImpl;
import com.greg.gamedesire.server.persistence.IEntitiesContainerDao;
import com.greg.gamedesire.server.persistence.MatchesDAO;
import com.greg.gamedesire.shared.ISharedUtility;
import com.greg.gamedesire.shared.SharedUtilityImpl;

public class GamedesireModule extends AbstractModule 
{
	@Override
	protected void configure() 
	{
		bind(IMatchesExtractor.class).to(MatchesExtractorImpl.class);
		bind(IHTMLParser.class).to(HTMLParserImpl.class);
		bind(IEntitiesContainerDao.class).to(MatchesDAO.class);
		bind(ISharedUtility.class).to(SharedUtilityImpl.class);
		bind(RemoteApiServlet.class).in(Singleton.class);
		bind(IMatchesFilter.class).to(MatchesFilterImpl.class);
		
		requestStaticInjection(ServerUtility.class);
	}
}
