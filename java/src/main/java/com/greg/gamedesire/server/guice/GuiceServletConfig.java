package com.greg.gamedesire.server.guice;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import com.greg.gamedesire.server.HandlerModule;

public class GuiceServletConfig extends GuiceServletContextListener 
{
	private static Injector INJECTOR;

	@Override
	protected Injector getInjector() 
	{
		if(INJECTOR == null) 
		{
			INJECTOR = Guice.createInjector(new HandlerModule(), new GamedesireModule(), new DispatchServletModule());
		}
	
		return INJECTOR;
	}
	
}