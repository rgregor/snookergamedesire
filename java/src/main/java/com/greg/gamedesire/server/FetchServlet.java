package com.greg.gamedesire.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.greg.gamedesire.server.impl.MatchesExtractorImpl;
import com.greg.gamedesire.shared.dto.Match;

@Singleton
public class FetchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(FetchServlet.class.getName());
	private IMatchesExtractor extractor;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	@Inject
    public FetchServlet(IMatchesExtractor extractor) {
        super();
        this.extractor = extractor;
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> favorites = ServerUtility.getFavorites(getServletContext());
		StringBuffer sb = new StringBuffer();
		
		for (String favorite : favorites) {
			List<Match> matches = new ArrayList<Match>();
			try {		
				List<Match> savedMatches = extractor.fetchAllMatches();
				matches = extractor.fetchMatchResultPages(favorite, savedMatches);
				log.severe(matches.size() + " entries saved for " + favorite);
			} catch (Exception e) {
				log.severe("Error saving entries for " + favorite + " : " + e.getMessage());
				e.printStackTrace();
			} 
        	  sb.append(matches.size() + " Matches gefunden:\n");
        	  for (int i=0; i<matches.size();i++) {
        		  sb.append(i+1).append(": ").append(matches.get(i)).append("\n");
        	  }
        	sb.append("\n\n");
			
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
