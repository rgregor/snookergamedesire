package com.greg.gamedesire.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.SimpleTimeZone;
import java.util.logging.Logger;

import javax.servlet.ServletContext;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import com.greg.gamedesire.server.persistence.IEntitiesContainerDao;
import com.greg.gamedesire.shared.dto.Match;

public class ServerUtility {
	
	private static final Logger log = Logger.getLogger(ServerUtility.class.getName());
	
	public static List<String> getFavorites(ServletContext servletContext) throws IOException {
		InputStream input = servletContext.getResourceAsStream("/favorites.txt");
		String favoritesLine = new BufferedReader(new InputStreamReader(input)).readLine();
		List<String> favorites = Arrays.asList(favoritesLine.split(","));
		
		return favorites;
	}

}
