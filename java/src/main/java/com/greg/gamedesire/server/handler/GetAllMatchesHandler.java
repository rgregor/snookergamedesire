package com.greg.gamedesire.server.handler;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Logger;

import net.customware.gwt.dispatch.server.ActionHandler;
import net.customware.gwt.dispatch.server.ExecutionContext;
import net.customware.gwt.dispatch.shared.ActionException;
import net.customware.gwt.dispatch.shared.DispatchException;

import com.google.inject.Inject;
import com.greg.gamedesire.server.IMatchesExtractor;
import com.greg.gamedesire.shared.dispatch.action.GetAllMatches;
import com.greg.gamedesire.shared.dispatch.result.GetAllMatchesResult;
import com.greg.gamedesire.shared.dto.Match;

public class GetAllMatchesHandler implements ActionHandler<GetAllMatches, GetAllMatchesResult> {
	private static final Logger log = Logger.getLogger(GetAllMatchesHandler.class.getName());
	private IMatchesExtractor extractor;

	@Inject
	public GetAllMatchesHandler(IMatchesExtractor extractor)
	{
		this.extractor = extractor;
	}

	@Override
	public GetAllMatchesResult execute(GetAllMatches action,
			ExecutionContext context) throws DispatchException 
	{
		ArrayList<Match> matches = new ArrayList<Match>();
		
		try 
		{
			matches = extractor.getMatchResults();
			log.info("Server returns " + matches.size() + " Matches");
		} 
		catch (ParseException e) 
		{
			log.severe(e.getMessage());
			throw new ActionException("Parsing Exception");		
		}	
		catch (Exception e) 
		{
			log.severe(e.getMessage());
			throw new ActionException(e.getMessage());
		}
		
		return new GetAllMatchesResult(matches);
	}
	
	@Override
	public void rollback(GetAllMatches action, GetAllMatchesResult result,
			ExecutionContext context) throws DispatchException {}
	
	@Override
	public Class<GetAllMatches> getActionType() 
	{
		return GetAllMatches.class;
	}

}
