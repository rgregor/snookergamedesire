package com.greg.gamedesire.server;

import net.customware.gwt.dispatch.server.guice.ActionHandlerModule;

import com.greg.gamedesire.server.handler.GetAllMatchesHandler;
import com.greg.gamedesire.shared.dispatch.action.GetAllMatches;


public class HandlerModule extends ActionHandlerModule 
{
	@Override
	protected void configureHandlers() 
	{
		bindHandler(GetAllMatches.class, GetAllMatchesHandler.class);
	}
}
