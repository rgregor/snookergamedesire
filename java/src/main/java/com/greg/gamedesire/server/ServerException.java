package com.greg.gamedesire.server;

public class ServerException extends RuntimeException 
{
	public ServerException(String message)
	{
		super(message);
	}
	
	public ServerException(Exception e)
	{
		super(e);
	}

	public ServerException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
