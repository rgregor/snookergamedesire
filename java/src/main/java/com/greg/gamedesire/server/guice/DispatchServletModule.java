package com.greg.gamedesire.server.guice;

import com.google.apphosting.utils.remoteapi.RemoteApiServlet;
import com.google.inject.servlet.ServletModule;
import com.greg.gamedesire.server.FetchServlet;
import com.greg.gamedesire.server.MatchesServlet;

public class DispatchServletModule extends ServletModule 
{
	public static String DISPATCH_URL = "/dispatch";
	
	@Override
	public void configureServlets() 
	{
		serve(DISPATCH_URL).with(GuiceMobileDispatchServlet.class);
		serve("/matches").with(MatchesServlet.class);
		serve("/fetch").with(FetchServlet.class);
		
		serve("/my-remote-api").with(RemoteApiServlet.class);
	}
}

