package com.greg.gamedesire.server;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.customware.gwt.dispatch.shared.ActionException;

import java.text.ParseException;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.greg.gamedesire.shared.dto.Match;

public interface IMatchesExtractor {

	public ArrayList<Match> extractMatches(String result, List<Date> dates)
			throws ParseException;

	public ArrayList<Match> getMatchResults() throws ParseException,
			FailingHttpStatusCodeException, MalformedURLException, IOException,
			InterruptedException;

	public final String matchesUrl = "http://www.gamedesire.com/index.html?dd=16&n=2&sub=1&gg=103&mod_name=player_results&player=%s&view=show&page_size=100";

	List<Match> fetchAllMatches();

	ArrayList<Match> fetchMatchResultPages(String user, List<Match> savedMatches)
			throws ParseException, ActionException,
			FailingHttpStatusCodeException, MalformedURLException, IOException,
			InterruptedException;

}
