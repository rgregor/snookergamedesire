package com.greg.gamedesire.server.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.CookieManager;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSpan;
import com.greg.gamedesire.server.IHTMLParser;
import com.greg.gamedesire.server.ServerException;

public class HTMLParserImpl implements IHTMLParser
{
	private static final Logger log = Logger.getLogger(HTMLParserImpl.class.getName());
	
	@Override
	public String parse(String path) throws ServerException {
		try {
			URL gamedesireUrl = new URL(path);
			BufferedReader in = new BufferedReader(
						new InputStreamReader(
						gamedesireUrl.openStream()));
	
			String inputLine;
			StringBuffer sb = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) sb.append(inputLine);
	
			in.close();
			
			return sb.toString();
			
		} catch (MalformedURLException e) {
			throw new ServerException(e.getMessage()); 
		} catch (IOException e) {
			throw new ServerException(e.getMessage()); 
		}
	}
	@Override
	public List<Date> getMatchDates(String path) throws FailingHttpStatusCodeException, MalformedURLException, IOException, InterruptedException {
		WebClient client = new WebClient(BrowserVersion.FIREFOX_3_6);
		client.setCookieManager(new CookieManager() {
			protected int getPort(final URL url) {
				final int r = super.getPort(url);
				return r != -1 ? r : 80;
			}
		});
		client.getCookieManager().setCookiesEnabled(false);
		client.getOptions().setThrowExceptionOnFailingStatusCode(false);
		client.getOptions().setThrowExceptionOnScriptError(false);
		
		WebRequest request = new WebRequest(new URL(path));
		HtmlPage page = client.getPage(request);
		
		client.waitForBackgroundJavaScript(10000);
		log.severe("After having waited");
		String dateContainerLocator = "//*[@class='archieve_table']/*[@class='hoverTable']/tbody/tr/td[2]/span";
		final List<?> dateContainers = page.getByXPath(dateContainerLocator);
		
		List<Date> dates = new ArrayList<Date>();
		for (Object element : dateContainers) {
			try {HtmlSpan dateContainer = (HtmlSpan) element;
		    DateFormat formatterGer = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.GERMAN);
		    DateFormat formatterGer2 = new SimpleDateFormat("dd MM yyyy HH:mm:ss", Locale.GERMAN);
		    DateFormat formatterEng = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		    DateFormat formatterEng2 = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss", Locale.ENGLISH);
		    Date date = null;
			
				final String dateString = dateContainer.getTextContent().trim();
				try {
					date = (Date)formatterEng.parse(dateString);
				} catch (Exception e) {
					try {
						date = (Date)formatterGer.parse(dateString);
					} catch (Exception e1) {
						try {
							date = (Date)formatterGer2.parse(dateString);
						} catch (Exception e2) {
							try {
								date = (Date)formatterEng2.parse(dateString);
							} catch (Exception e3) {
								throw new RuntimeException("ERROR: Date could not be parsed in getMatchDates: " + dateString);
							}
							
						}
					}
				}
				dates.add(date);
			} catch (Exception e) {
				log.severe("htmlunit-exception: " + e.getMessage());
				e.printStackTrace();
			}
		   
		}
		return dates;
		
	}

}
