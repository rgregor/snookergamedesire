package com.greg.gamedesire.server.persistence;


import java.util.List;
import java.util.Map;
import java.util.Set;

import com.googlecode.objectify.Key;

public interface IEntitiesContainerDao<T, E> {

	public abstract void add(List<E> entities);

	public abstract List<E> getAll();

	public abstract void handleOldMatches();	

}
