package com.greg.gamedesire.server.persistence;

import java.util.logging.Logger;

import com.googlecode.objectify.ObjectifyService;
import com.greg.gamedesire.shared.dto.ContainerTable;
import com.greg.gamedesire.shared.dto.Match;
import com.greg.gamedesire.shared.dto.MatchesContainerImpl;

public class MatchesDAO extends AbstractDAOImpl<MatchesContainerImpl, Match> {
	private static final Logger LOG = Logger.getLogger(MatchesDAO.class.getName());

	static
	{
		ObjectifyService.register(MatchesContainerImpl.class);
//		ObjectifyService.register(Match.class);
		ObjectifyService.register(ContainerTable.class);
	}

	public MatchesDAO()
	{
		super(MatchesContainerImpl.class, Match.class);
	}




}
