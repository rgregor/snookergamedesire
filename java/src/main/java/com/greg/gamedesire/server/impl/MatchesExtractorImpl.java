package com.greg.gamedesire.server.impl;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.google.inject.Inject;
import com.greg.gamedesire.server.IHTMLParser;
import com.greg.gamedesire.server.IMatchesExtractor;
import com.greg.gamedesire.server.ValueContainer;
import com.greg.gamedesire.server.persistence.IEntitiesContainerDao;
import com.greg.gamedesire.shared.EndType;
import com.greg.gamedesire.shared.ISharedUtility;
import com.greg.gamedesire.shared.dto.Match;
import com.greg.gamedesire.shared.dto.MatchesContainerImpl;
import net.customware.gwt.dispatch.shared.ActionException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

public class MatchesExtractorImpl implements IMatchesExtractor 
{
	
	private static final Logger log = Logger.getLogger(MatchesExtractorImpl.class.getName());
	private ISharedUtility utility;
	private IEntitiesContainerDao<MatchesContainerImpl, Match> matchesDAO;
	private IHTMLParser parser;
	
	@SuppressWarnings("unchecked")
	@Inject
	public MatchesExtractorImpl(IHTMLParser parser, ISharedUtility utility, IEntitiesContainerDao matchesDao)
	{
		this.utility = utility;
		this.matchesDAO = matchesDao;
		this.parser = parser;
	}
	
	@Override
	public List<Match> fetchAllMatches() {		
		return matchesDAO.getAll();
//		matchesDAO.handleOldMatches();
//		
//		return new ArrayList<Match>();
	}
	
	@Override
	public ArrayList<Match> fetchMatchResultPages(String user, List<Match> savedMatches) throws ParseException, ActionException, FailingHttpStatusCodeException, MalformedURLException, IOException, InterruptedException 
	{		 
		if(savedMatches == null) {
			return new ArrayList<Match>();
		}
		log.info(savedMatches.size() + " fetched from db");
		String userMatchesUrl = matchesUrl.replace("%s", user);
		List<Match> newMatches = mineMatchResultPages(userMatchesUrl, savedMatches, new ArrayList<Match>());

		log.info(newMatches.size() + " matches fetched for " + user);
		
		matchesDAO.add(newMatches);
		
		
		log.info(newMatches.size() + " matches persisted for " + user);
		
		ArrayList<Match> allMatches = new ArrayList<Match>(utility.extractMatchesOfPlayer(savedMatches, user));
		allMatches.removeAll(newMatches);
		allMatches.addAll(newMatches);
		
		StringBuilder sb = new StringBuilder("new matches for " + user + ": ");
		for (Match match : newMatches) {
			sb.append(match.toStringDeluxe());
		}
		log.severe(sb.toString());
		
		Collections.sort(allMatches);
		Collections.reverse(allMatches);
		
		return new ArrayList<Match>(allMatches);
	}
	
	@Override
	public ArrayList<Match> getMatchResults() throws ParseException, FailingHttpStatusCodeException, MalformedURLException, IOException, InterruptedException 
	{		
		List<Match> savedMatches = matchesDAO.getAll();
		
		log.fine(savedMatches.size() + " fetched from db");
	
		Collections.sort(savedMatches);
		Collections.reverse(savedMatches);
		
		return new ArrayList<Match>(savedMatches);
	}
	
	@Override
	public ArrayList<Match> extractMatches(String result, List<Date> dates) throws ParseException  {		
	    
	    /** start logic **/

	    String rowSeparator = "</td></tr></table></td></tr>";
	    int startFirstLine = result.indexOf("<tbody>");
	    result = result.substring(startFirstLine);
	    
	    List<String> rows = new ArrayList<String>(Arrays.asList(result.split(rowSeparator)));
	    rows.remove(rows.size()-1);
	    
	    ArrayList<Match> matches = new ArrayList<Match>();
	    int i=0;
	    
	    for (String row : rows) 
	    {
	    	Match match = new Match();
	    	match.date = dates.get(i++);
	    	ValueContainer<Date> dateContainer = parseDate(row);
	    	row = dateContainer.remainingString;

	    	ValueContainer<Integer> durationContainer = parseDuration(row);
	    	row = durationContainer.remainingString;	    	
	    	match.duration = durationContainer.field;
	    	
	    	ValueContainer<String> player1Container = parsePlayer(row);
	    	row = player1Container.remainingString;	    	
	    	match.player1 = player1Container.field;
	    	
	    	ValueContainer<String> player2Container = parsePlayer(row);
	    	row = player2Container.remainingString;	    	
	    	match.player2 = player2Container.field;
	    	
	    	ValueContainer<String> winnerContainer = parseWinner(row);
	    	row = winnerContainer.remainingString;
	    	match.winner = winnerContainer.field;	    	
	    	
	    	if(ranking1isParsable(row)) {
	        	ValueContainer<Double> ranking1Container = parseRanking1(row);
		      	row = ranking1Container.remainingString;
		    	match.ranking1 = ranking1Container.field;
		    	
		    	row = executeWhenValuedGame(row, match);
	    	}
	    	else {
	    		match.ranking1 = 0;
	    		match.difference1 = 0;
	    		match.ranking2 = 0;
	    		match.difference2 = 0;
	    		
	    		row = subStringFromString(row, "<font");
	    		row = subStringFromString(row, ">");
	    		row = subStringFromString(row, "</td>");
	    		row = subStringFromString(row, "</tbody></table></td>");
	    	}	
	    	
	    	ValueContainer<Boolean> ratedContainer = parseRated(row);
	    	row = ratedContainer.remainingString;
	    	match.rated = ratedContainer.field;
	    	
	    	ValueContainer<EndType> endtypeContainer = parseEndType(row);
	    	row = endtypeContainer.remainingString;
	    	match.endType = endtypeContainer.field;
	    	
	    	ValueContainer<Integer> maxBreak1Container = parseMax1(row);
	    	row = maxBreak1Container.remainingString;
	    	match.maxBreak1 = maxBreak1Container.field;
	    	
	    	ValueContainer<Integer> maxBreak2Container = parseMax2(row);
	    	row = maxBreak2Container.remainingString;
	    	match.maxBreak2 = maxBreak2Container.field;
 	    
	    	matches.add(match);
	    	
	    	
	    } 
	    
	    log.info(rows.size() + " Spiele gefunden!");

		return matches;
		
	}

	private String executeWhenValuedGame(String row, Match match) {
		ValueContainer<Double> difference1Container = parseDifference(row);
		row = difference1Container.remainingString;
		match.difference1 = difference1Container.field;
		
		ValueContainer<Double> ranking2Container = parseRanking2(row);
		row = ranking2Container.remainingString;
		match.ranking2 = ranking2Container.field;	    	
		
		ValueContainer<Double> difference2Container = parseDifference(row);
		row = difference2Container.remainingString;
		match.difference2 = difference2Container.field;
		return row;
	}
	
	private ValueContainer<Integer> parseMax2(String row) {
		row = subStringFromString(row, "<td>");
		int maxBreak = Integer.parseInt(row);
		return new ValueContainer<Integer>(maxBreak, "");
	}

	private ValueContainer<Integer> parseMax1(String row) 
	{
		
		row = subStringFromString(row, "<font");
		row = subStringFromString(row, ">");
		int maxBreak;
		try {
			maxBreak = Integer.parseInt(subStringToString(row, "</fon"));
		} catch (NumberFormatException e) {
			maxBreak = 0;
		}
		return new ValueContainer<Integer>(maxBreak, subStringFromString(row, "</fon"));
	}

	private ValueContainer<EndType> parseEndType(String row) 
	{
		row = subStringFromString(row, "\">");
		String endtype = subStringToString(row, "</td>");
		return new ValueContainer<EndType>(EndType.asEnumType(endtype), subStringFromString(row, "</td>"));
	}

	private ValueContainer<Boolean> parseRated(String row) 
	{
		row = subStringFromString(row, "<td");
		row = subStringFromString(row, "\">");
		final String ratedString = subStringToString(row, "</td>");
		boolean rated = (ratedString.equals("JA") || ratedString.equals("YES")) ? true : false; 
		return new ValueContainer<Boolean>(rated, subStringFromString(row, "</td>"));
	}

	private ValueContainer<Double> parseDifference(String row) {
		row = subStringFromString(row, "(");
		double difference;
		try {
			difference = Double.parseDouble(subStringToString(row, ")").replace(",", "."));
		} catch (NumberFormatException e) {
			difference = 0;
		}
		return new ValueContainer<Double>(difference, subStringFromString(row, ")"));
	}
	
	private ValueContainer<Double> parseRanking2(String row) {
		row = subStringFromString(row, "<td");
		row = subStringFromString(row, "\">");
		double ranking;
		try {
			ranking = Double.parseDouble(subStringToString(row, "</td>"));
		} catch (NumberFormatException e) {
			ranking = 0;
		}
		return new ValueContainer<Double>(ranking, subStringFromString(row, "</td>"));
	}

	private ValueContainer<Double> parseRanking1(String row) {
		row = subStringFromString(row, "<font");
		row = subStringFromString(row, ">");
		double ranking;
		try {
			ranking = Double.parseDouble(subStringToString(row, "</td>"));
		} catch (NumberFormatException e) {
			ranking = 0;
		}
		return new ValueContainer<Double>(ranking, subStringFromString(row, "</td>"));
	}
	
	private boolean ranking1isParsable(String row) {
		row = subStringFromString(row, "<font");
		row = subStringFromString(row, ">");
		double ranking;
		try {
			ranking = Double.parseDouble(subStringToString(row, "</td>"));
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private ValueContainer<String> parseWinner(String row) {
		row = subStringFromString(row, "<b>");
		String winner = subStringToString(row, "</b>");
		return new ValueContainer<String>(winner, subStringFromString(row, "</b>"));
		
	}

	private ValueContainer<String> parsePlayer(String row) {
		row = subStringFromString(row, "<span");
		row = subStringFromString(row, ">");
		String player = subStringToString(row, "</span>");
		return new ValueContainer<String>(player, subStringFromString(row, "</span>"));
	}

	private ValueContainer<Date> parseDate(String row) throws ParseException {
		
			row = subStringFromString(row, "<span");
	    	row = subStringFromString(row, "\">");
	    	String dateString = subStringToString(row, "</span>");

		    DateFormat formatter = new SimpleDateFormat("dd MMMMM yyyy HH:mm:ss", Locale.US);
		    DateFormat formatterGer = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss", Locale.GERMAN);
		    Date date = null;
			try {
				date = (Date)formatter.parse(dateString);
			} catch (Exception e) {
				try {
					date = (Date)formatterGer.parse(dateString);
				} catch (Exception e1) {
					throw new RuntimeException("Date could not be parsed in parseDate: " + dateString);
				}
			}
		    
		    return new ValueContainer<Date>(date, subStringFromString(row, "</span>"));
	}
	
	private  ValueContainer<Integer> parseDuration(String row) {
		row = subStringFromString(row, "\">");
		String time = subStringToString(row, "</td>");
		String[] minSec = time.split(":");
		int seconds = Integer.parseInt(minSec[0]) * 60 + Integer.parseInt(minSec[1]);
		
		return new ValueContainer<Integer>(seconds, subStringFromString(row,"</td>"));	
	}

	private String subStringFromString(String row, String start) {
		return row.substring(row.indexOf(start)+start.length());
	}
	
	private String subStringToString(String row, String end) {
		String substring="";
		try {
			substring = row.substring(0, row.indexOf(end));
		} catch (StringIndexOutOfBoundsException e) {
			// do nothing
		}
		return substring;
	}

	private List<Match> mineMatchResultPages(String url, List<Match> savedMatches, List<Match> matchesFound) throws ParseException, ActionException, FailingHttpStatusCodeException, MalformedURLException, IOException, InterruptedException {
		log.fine("Entering mineMatchResultPages");
		String result="";		

		result = parser.parse(url);	
		
		String matchesTable = extractMatchesTable(result);
		log.fine("matchesTable startswith " + matchesTable.substring(0,15));
		
		List<Date> dates = parser.getMatchDates(url);
		log.fine(dates.size() + " daten gefunden");
		ArrayList<Match> matches = extractMatches(matchesTable,dates);
		log.fine(matches.size() + " matches gefunden");
		matches.removeAll(savedMatches);
		
		matchesFound.addAll(matches);

        log.fine("Result at critical position:");
        String intermediateResult = subStringFromString(result, "pagin_pagin");
		intermediateResult = subStringToString(intermediateResult, "</tr>");
        log.fine(intermediateResult);

        if(!matches.isEmpty() && otherMatchesPageExistant(result)) {
			return mineMatchResultPages(getUrlOfNextPage(result), savedMatches, matchesFound);
		}
		else {
			log.fine("No other match pages found!");
			return matchesFound;
		}
	}
	
	private String getUrlOfNextPage(String result) {
		result = subStringFromString(result, "pagin_pagin");
		result = subStringToString(result, "</tr>");

        String url = null;
        if(result.contains("&raquo;")) {
            int lastHref = lastPositionOf("href=", result);
            String approx = result.substring(lastHref);
            approx = approx.substring(6);
            approx = subStringToString(approx, "\"");
            url = "http://www.gamedesire.com" + approx;
            log.fine(url + " will be loaded next!");
        } else {
            log.fine("Does not contain a raquo");
        }

		return url;
	}

	private int getNumberOccurrences(String string, String result) {
		int number=0;
		while(result.contains(string)) {
			number++;
			result = result.substring(result.indexOf(string)+string.length());
		}
		log.fine(number + " occurrences!");
		return number;
	}

	private boolean otherMatchesPageExistant(String result) {
//		return false;

        return getUrlOfNextPage(result) != null;
		
	}

	private int lastPositionOf(String pattern, String result) {
		String result_reversed = new StringBuffer(result).reverse().toString();
		String pattern_reversed = new StringBuffer(pattern).reverse().toString();
		return result_reversed.length() - result_reversed.indexOf(pattern_reversed) - pattern.length();
	}

	private String extractMatchesTable(String result) {
		String hoverTable = "<div class=\"archieve_table\"";
		String tableFinished = "</tbody></table></div>";
		int startOfTable = result.indexOf(hoverTable);
		String startAtTable = result.substring(startOfTable);
		int endOfTable = startAtTable.indexOf(tableFinished);
		
		String resultTable = startAtTable.substring(hoverTable.length(), endOfTable+tableFinished.length());
		return resultTable;
	}

}
