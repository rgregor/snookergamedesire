package com.greg.gamedesire.server.persistence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.googlecode.objectify.ObjectifyOpts;
import com.googlecode.objectify.util.DAOBase;
import com.greg.gamedesire.server.ServerException;
import com.greg.gamedesire.shared.dto.ContainerTable;
import com.greg.gamedesire.shared.dto.IEntitiesContainer;
import com.greg.gamedesire.shared.dto.Match;
import com.greg.gamedesire.shared.dto.MatchesContainerImpl;

public abstract class AbstractDAOImpl<T extends IEntitiesContainer<E>, E extends IsSerializable> extends DAOBase implements IEntitiesContainerDao<T, E> 
{
	private static final Logger log = Logger.getLogger(AbstractDAOImpl.class.getName());
	private Class<T> containerClass;
	private Class<E> entityClass;

	protected AbstractDAOImpl(Class<T> containerClass, Class<E> entityClass)
	{
		super(new ObjectifyOpts().setSessionCache(false));
		this.containerClass = containerClass;
		this.entityClass = entityClass;
	}

	public void delete(T entity)
	{
		ofy().delete(entity);
	}

	public List<E> getAll()
	{
		List<E> entities = new ArrayList<E>();
		try {
//			entities = ofy().query(this.clazz).chunkSize(5000).prefetchSize(5000).list();
			ContainerTable containerTable = ofy().query(ContainerTable.class).get();
			if(containerTable == null) {
				containerTable = new ContainerTable();
				ofy().put(containerTable);
			}
			
			List<T> containers = getContainers(containerTable);
			
			for (T t : containers) {
				entities.addAll(t.getEntities());
			}
			
			log.info("Size of entities " + entities.size());
		} 
		catch (NotFoundException e) {
			log.severe(e.getMessage());
			return entities;
		}
		catch (Exception e) {
			log.severe(String.format("getAll(): %s", e.getMessage()));
			throw new ServerException(e);
		}
		
		return entities;
	}

	private List<T> getContainers(ContainerTable containerTable) {
		List<T> containers = new ArrayList<T>();
		
		List<Long> containerIds = containerTable.getContainers();		
		if(! containerIds.isEmpty()) {
			Map<Long, T> containerMaps = ofy().get(this.containerClass, containerIds);
			containers.addAll(containerMaps.values());
		}		
	
		return containers;
	}
	
	@Override
	public void add(List<E> entities)
	{
		ContainerTable containerTable = ofy().query(ContainerTable.class).get();
		if(containerTable == null) {
			containerTable = new ContainerTable();
			ofy().put(containerTable);
		}
		
		List<T> containers = getContainers(containerTable);
		
		List<T> notFullContainers = new ArrayList<T>();
		for (T t : containers) {
			if(t.getEntities().size() < t.getMaxNumberEntities()) {
				notFullContainers.add(t);
			}
		}
		
		int remainingEntities = entities.size();
		int startIndex = 0;
		if(! notFullContainers.isEmpty()) {
			T containerToFill = notFullContainers.get(0);
			int finalIndex;
			
			int freeSpace = containerToFill.getMaxNumberEntities() - containerToFill.getEntities().size();
			if(entities.size() < freeSpace) {
				finalIndex = entities.size();
			}
			else {
				finalIndex = freeSpace;
			}
			containerToFill.addEntities(new ArrayList<E>(entities.subList(0, finalIndex)));
			ofy().put(containerToFill);
			
			remainingEntities = remainingEntities - finalIndex;
			startIndex = finalIndex;
		}
		
		insertMatchesIntoContainers(entities, containerTable,
				remainingEntities, startIndex);
	}

	private void insertMatchesIntoContainers(List<E> entities,
			ContainerTable containerTable, int remainingEntities, int startIndex) {
		if(remainingEntities > 0) {
			IEntitiesContainer<E> newContainer = (IEntitiesContainer<E>) new MatchesContainerImpl();
			int finalIndex;
			if(entities.size() < startIndex + newContainer.getMaxNumberEntities()) {
				finalIndex = entities.size();
			}
			else {
				finalIndex = startIndex + newContainer.getMaxNumberEntities();
			}
			List<E> subList = new ArrayList<E>(entities.subList(startIndex, finalIndex));
			newContainer.addEntities(subList);
			Key<IEntitiesContainer<E>> key = ofy().put(newContainer);
			containerTable.addContainer(key.getId());
			ofy().put(containerTable);
			remainingEntities = remainingEntities - subList.size();
			startIndex = startIndex + subList.size();
			
			insertMatchesIntoContainers(entities, containerTable,
					remainingEntities, startIndex);
		}
		else {
			return;
		}
	}
	
	@Deprecated
	@Override
	public void handleOldMatches() {
		//List<E> entities = ofy().query(this.entityClass).chunkSize(5000).prefetchSize(5000).list();
	
		List<E> entities = (List<E>) new ArrayList<Match>(Arrays.asList(new Match("kallo"), new Match("lallo"), new Match("mallo")));
		if(! entities.isEmpty()) {
			add(entities);		
			//ofy().delete(entities);
		}
		
		
	}


}
