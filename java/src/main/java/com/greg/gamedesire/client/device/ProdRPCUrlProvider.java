package com.greg.gamedesire.client.device;


/**
 * Provides the RPC url used from the
 * mobile device.
 * 
 * @author Gregor Riegler
 *
 */
public class ProdRPCUrlProvider implements RPCUrlProvider
{

	@Override
	public String getRPCPrefix()
	{
		// will be changed to our app engine url
		return "http://snookergamedesire.appspot.com/";
	}

}
