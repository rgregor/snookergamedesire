package com.greg.gamedesire.client.resources;

import com.google.gwt.resources.client.CssResource;

public interface MyCSS extends CssResource {
	@ClassName(value="latest-matches-dates")
	String latestMatchesDates();
	
	String mainMenu();
	String menuLinkActive();
	String breakLine();
	String breakLineAreas();
	String bold();

}
