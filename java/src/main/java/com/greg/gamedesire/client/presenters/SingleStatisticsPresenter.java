package com.greg.gamedesire.client.presenters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.inject.Inject;
import com.greg.gamedesire.client.IGamedesireView;
import com.greg.gamedesire.client.SingleStatistics;
import com.greg.gamedesire.shared.ISharedUtility;
import com.greg.gamedesire.shared.dto.Match;

public abstract class SingleStatisticsPresenter<T extends IGamedesireView> extends WebPresenter<T> 
{
	@Inject
	protected ISharedUtility utility;
	
	protected List<Match> matches = Collections.emptyList();
	protected List<String> users = Collections.emptyList();
	private String wantedUser = "";
	
	public void onUsersFetched(List<String> users)
	{
		this.users = users;
		showStatisticsOfUser(getAvailableUser(wantedUser));
	}
	
	public void onMatchesFetched(ArrayList<Match> matches)
	{
		this.matches = matches;
		showStatisticsOfUser(getAvailableUser(wantedUser));
	}
	
	public void onShowMatches(String user)
	{
		onShowStatistics(user, SingleStatistics.MATCHES);
	}
	
	public void onShowRanking(String user)
	{
		onShowStatistics(user, SingleStatistics.RANKING);
	}
	
	public void onShowBreaks(String user)
	{
		onShowStatistics(user, SingleStatistics.BREAKS);
	}
	
	protected final void onShowAction(String user) {
		if(isReady())
		{
			String userAvailable = getAvailableUser(user);
			if(!user.equals(userAvailable))
				eventBus.dispatch(getType().getEventName(), userAvailable);
			else
			{
				showStatisticsOfUser(userAvailable);
			}
		}
		else
			setWantedUser(user);
		
		eventBus.setSingleUserStatistics(view.asWidget());
	}

	private void showStatisticsOfUser(String user)
	{
		if(isReady()) 
		{
			displayStatisticsOfUser(user);
			eventBus.selectSingleUser(user);
		}		
	}
	
	protected abstract void displayStatisticsOfUser(String user);
	
	protected abstract SingleStatistics getType();
	
	protected final boolean isReady() {
		return !matches.isEmpty() && !users.isEmpty();
	}
	
	private void onShowStatistics(String user, SingleStatistics statisticsType)
	{
		setWantedUser(user);
		
		if(statisticsType == getType())
			onShowAction(user);		
	}

	private void setWantedUser(String user) {
		if(utility.hasValue(user))
			wantedUser = user;
	}
	
	private String getAvailableUser(String user)
	{
		if(utility.hasValue(user))
			return user;
		if(utility.hasValue(wantedUser))
			return wantedUser;
		else
			return users.get(0);			
	}
}
