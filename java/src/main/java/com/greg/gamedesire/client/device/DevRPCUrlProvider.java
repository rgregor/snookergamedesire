package com.greg.gamedesire.client.device;

/**
 * Provides the RPC prefix used in dev mode - an empty string.
 * 
 * @author Gregor Riegler
 * 
 */
public class DevRPCUrlProvider implements RPCUrlProvider
{

	@Override
	public String getRPCPrefix()
	{
		return "";
	}

}
