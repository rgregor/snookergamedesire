package com.greg.gamedesire.client.views;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.greg.gamedesire.client.presenters.SelectUserPresenter.ISelectUserView;

public class SelectUserView extends Composite implements ISelectUserView 
{
	protected static IUiBinder uiBinder = GWT.create(IUiBinder.class);

	interface IUiBinder extends UiBinder<Widget, SelectUserView> {
	}
	

	@UiField
	ListBox selectUser;

	public SelectUserView() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public HasChangeHandlers getDropDown() {
		return selectUser;
	}

	@Override
	public void setUsers(List<String> users) {
		selectUser.clear();
		for (String user : users) {
			selectUser.addItem(user);
		}

	}

	@Override
	public void reset() {		
	}

	@Override
	public String getSelectedUser() {
		int selectedIndex = selectUser.getSelectedIndex();
		if(selectedIndex != -1)
			return selectUser.getValue(selectedIndex);
		else
			return "";
	}

	@Override
	public void selectUser(int index) 
	{
		if(index >= 0)
			selectUser.setSelectedIndex(index);
	}

}
