package com.greg.gamedesire.client.presenters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.greg.gamedesire.client.IGamedesireView;
import com.greg.gamedesire.client.views.LatestMatchesView;
import com.greg.gamedesire.shared.dto.Match;
import com.mvp4g.client.annotation.Presenter;

@Presenter(view = LatestMatchesView.class)
public class LatestMatchesPresenter extends WebPresenter<LatestMatchesPresenter.ILatestMatchesView> {

	public interface ILatestMatchesView extends IGamedesireView {

		void showMatches(Map<Date, List<Match>> dateToMatches);

	}

	private List<Match> matches = Collections.emptyList();

	public void onShowLatestMatches() {
		eventBus.changeContent(view.asWidget());
	}

	public void onMatchesFetched(ArrayList<Match> matches) {
		this.matches = matches;
		initializeView();
	}

	@SuppressWarnings("serial")
	private void initializeView() {
		Map<Date, List<Match>> dateToMatches = new HashMap<Date, List<Match>>();
		for (final Match match : matches) {
			Date date = new Date(match.date.getYear(), match.date.getMonth(), match.date.getDate());
			if (dateToMatches.containsKey(date))
				dateToMatches.get(date).add(match);
			else
				dateToMatches.put(date, new ArrayList<Match>(){{add(match);}});
		}
		
		view.showMatches(dateToMatches);	
		
	}
}
