package com.greg.gamedesire.client;

import com.mvp4g.client.annotation.History;
import com.mvp4g.client.annotation.History.HistoryConverterType;
import com.mvp4g.client.history.HistoryConverter;

@History(type = HistoryConverterType.SIMPLE)
public class GamedesireHistoryConverter implements
		HistoryConverter<GamedesireEventBus> {

	@Override
	public void convertFromToken(String historyName, String param,
			GamedesireEventBus eventBus) {
		eventBus.dispatch(historyName);

	}
	
	public String convertToToken(String token) {
		return "";
	}

	@Override
	public boolean isCrawlable() {
		return false;
	}
}
