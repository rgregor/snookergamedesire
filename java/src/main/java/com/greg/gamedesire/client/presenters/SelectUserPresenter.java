package com.greg.gamedesire.client.presenters;

import java.util.Collections;
import java.util.List;

import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.greg.gamedesire.client.IGamedesireView;
import com.greg.gamedesire.client.views.SelectUserView;
import com.mvp4g.client.annotation.Presenter;

@Presenter(view = SelectUserView.class, multiple = true)
public class SelectUserPresenter extends
		WebPresenter<SelectUserPresenter.ISelectUserView> 
{

	public interface ISelectUserView extends IGamedesireView {

		HasChangeHandlers getDropDown();
		
		String getSelectedUser();

		void setUsers(List<String> users);

		void selectUser(int index);
	}

	private List<String> users = Collections.emptyList();
	
	void setUsers(List<String> users)
	{
		this.users = users;
		view.setUsers(users);
	}
	
	void selectUser(String user)
	{
		int index = getIndexOf(user);
		view.selectUser(index);
	}

	HandlerRegistration addChangeHandler(ChangeHandler changeHandler) {
		return view.getDropDown().addChangeHandler(changeHandler);		
	}
	
	/**
	 * Get index of <code>user</code> in field <code>users</code>.
	 * 
	 * @return index of user in users, -1 when users don't contain user
	 */
	private int getIndexOf(String user) {
		if(users.contains(user))
		{
			int i=0;
			while(i < users.size())
			{
				if(user.equals(users.get(i)))
					break;
				else
					i++;
			}
			return i;
		}
		else
			return -1;
	}
}
