package com.greg.gamedesire.client.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.greg.gamedesire.client.presenters.SingleUserPresenter.ISingleUserView;

public class SingleUserView extends Composite implements ISingleUserView {
	private static IUiBinder	uiBinder	= GWT.create(IUiBinder.class);

	interface IUiBinder extends UiBinder<Widget, SingleUserView>
	{
	}
	
	@UiField Panel singleUserStatistics;
	@UiField Panel selectUserWrapper;
	
	public SingleUserView()
	{
		initWidget(uiBinder.createAndBindUi(this));	
	}

	@Override
	public void setUserSelectable(IsWidget widget)
	{
		selectUserWrapper.add(widget);
	}

	@Override
	public void setSingleUserStatistics(IsWidget widget) {
		singleUserStatistics.clear();
		singleUserStatistics.add(widget);
	}
	
	@Override
	public void reset() {		
	}

}
