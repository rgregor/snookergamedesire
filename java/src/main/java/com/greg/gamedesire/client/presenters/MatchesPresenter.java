package com.greg.gamedesire.client.presenters;

import java.util.List;

import com.greg.gamedesire.client.IGamedesireView;
import com.greg.gamedesire.client.SingleStatistics;
import com.greg.gamedesire.client.views.MatchesView;
import com.greg.gamedesire.shared.dto.Match;
import com.mvp4g.client.annotation.Presenter;

@Presenter(view = MatchesView.class)
public class MatchesPresenter extends
SingleStatisticsPresenter<MatchesPresenter.IMatchesView> {

	public interface IMatchesView extends IGamedesireView {
		void showMatches(List<Match> matchesOfPlayer);
	}
	
	@Override
	protected void displayStatisticsOfUser(String user) {
		showMatchesOfUser(user);		
	}
	
	private void showMatchesOfUser(final String username) {
		List<Match> matchesOfPlayer = utility.extractMatchesOfPlayer(matches, username);
		view.showMatches(matchesOfPlayer);
	}

	@Override
	protected SingleStatistics getType() {
		return SingleStatistics.MATCHES;
	}
}
