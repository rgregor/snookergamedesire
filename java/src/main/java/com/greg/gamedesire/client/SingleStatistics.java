package com.greg.gamedesire.client;

public enum SingleStatistics {
	RANKING("ranking"), MATCHES("matches"), BREAKS("breaks"), NONE("");
	
	private String eventName;

	SingleStatistics(String eventName)
	{
		this.eventName = eventName;
	}
	
	public String getEventName() {
		return eventName;
	}
}
