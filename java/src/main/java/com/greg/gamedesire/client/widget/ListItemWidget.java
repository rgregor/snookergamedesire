package com.greg.gamedesire.client.widget;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * GWT widget that corresponds to a HTML li tag.
 * Used in combination with {@link UnorderedListWidget}
 * More info: http://turbomanage.wordpress.com/2010/02/11/writing-plain-html-in-gwt/
 * 
 * @author Gregor Riegler
 *
 */
public class ListItemWidget extends SimplePanel
{
	public ListItemWidget()
	{
		super((Element) Document.get().createLIElement().cast());
	}

	public ListItemWidget(String s)
	{
		this();
		getElement().setInnerText(s);
	}

	public ListItemWidget(Widget w)
	{
		this();
		this.add(w);
	}
}

