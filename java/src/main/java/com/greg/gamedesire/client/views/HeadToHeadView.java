package com.greg.gamedesire.client.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.greg.gamedesire.client.presenters.HeadToHeadPresenter.IHeadToHeadView;

public class HeadToHeadView  extends Composite implements IHeadToHeadView
{
	protected static IUiBinder uiBinder = GWT.create(IUiBinder.class);

	interface IUiBinder extends UiBinder<Widget, HeadToHeadView> {
	}
	
	@UiField Panel selectUser1Wrapper, selectUser2Wrapper, pieChartWrapper;
	@UiField Label rankingChangesLabel;
	
	public HeadToHeadView() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void setSelectable1(IsWidget selectable) {
		selectUser1Wrapper.add(selectable);
		
	}
	@Override
	public void setSelectable2(IsWidget selectable) {
		selectUser2Wrapper.add(selectable);		
	}
	
	@Override
	public void setPieChart(IsWidget pie) {
		pieChartWrapper.clear();
		pieChartWrapper.add(pie);		
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showRankingChanges(float ranking1, float ranking2) 
	{
		String ranking1String = String.valueOf(ranking1);
		String ranking2String = String.valueOf(ranking2);
		if(ranking1 >= 0)
			ranking1String = "+" + ranking1String;
		if(ranking2 >= 0)
			ranking2String = "+" + ranking2String;
		ranking1String = shorten(ranking1String);
		ranking2String = shorten(ranking2String);
		rankingChangesLabel.setText("Ranking Change: " + ranking1String + " vs. " + ranking2String);
		
	}

	private String shorten(String string) {
		if(string.length()>=5)
			return string.substring(0, 4);
		else
			return string;
	}
}
