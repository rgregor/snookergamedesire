package com.greg.gamedesire.client.views;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.greg.gamedesire.client.presenters.LatestMatchesPresenter.ILatestMatchesView;
import com.greg.gamedesire.client.resources.MyResources;
import com.greg.gamedesire.shared.ISharedUtility;
import com.greg.gamedesire.shared.dto.Match;

public class LatestMatchesView extends Composite implements ILatestMatchesView {
	private static IUiBinder uiBinder = GWT.create(IUiBinder.class);

	interface IUiBinder extends UiBinder<Widget, LatestMatchesView> {
	}
	
	@Inject
	ISharedUtility utility;
	
	@UiField Panel latestPanel;
	

	public LatestMatchesView() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@Override
	public void reset() {
	}

	@Override
	public void showMatches(Map<Date, List<Match>> dateToMatches) {
		List<Date> dates = new ArrayList<Date>(dateToMatches.keySet());

		Collections.sort(dates);
		Collections.reverse(dates);
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd. MMMM YYYY");
		
		for (int i=0; i < 3; i++) {
			List<Match> matches = dateToMatches.get(dates.get(i));
			String dateString = dateFormat.format(dates.get(i));
			HTMLPanel matchesOfDateAsHTML = getMatchesOfDateAsHTML(dateString, matches);
			latestPanel.add(matchesOfDateAsHTML);
		}
			
	}

	private HTMLPanel getMatchesOfDateAsHTML(String date, List<Match> matches) {
		StringBuffer buffer;
		
		buffer = new StringBuffer();
		HTML dateHeadline = new HTML(date);
		dateHeadline.addStyleName(MyResources.INSTANCE.css().latestMatchesDates());
		buffer.append(dateHeadline.toString());
		for (Match match : matches) {				
			String matchString = matchToString(match);
			
			HTML matchToString = new HTML(matchString);
			buffer.append(matchToString.toString());
		}
		
		HTMLPanel matchesOfDate = new HTMLPanel(buffer.toString());
		return matchesOfDate;
	}

	private String matchToString(Match match) {
		String loser = utility.getLoser(match);		
		int maxBreak = utility.getMaxBreak(match);
		
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("HH:mm");

		return dateFormat.format(match.date) + ": " + match.winner + " wins against " + loser + ", the highest break was " + maxBreak;
	}


	


}
