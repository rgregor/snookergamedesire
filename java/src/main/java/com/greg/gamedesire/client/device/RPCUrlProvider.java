package com.greg.gamedesire.client.device;

/**
 * In devmode (browser) and in production (on android device) the
 * RPC url differs. This is the interface for implementations 
 * providing the correct url.
 * 
 * @author Gregor Riegler
 *
 */
public interface RPCUrlProvider
{
	String getRPCPrefix();
}
