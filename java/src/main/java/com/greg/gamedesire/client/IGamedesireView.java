package com.greg.gamedesire.client;

import com.google.gwt.user.client.ui.Widget;

public interface IGamedesireView {
	Widget asWidget();
	void reset();

}
