package com.greg.gamedesire.client.presenters;

import java.util.List;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.VisualizationUtils;
import com.google.gwt.visualization.client.visualizations.AnnotatedTimeLine;
import com.google.gwt.visualization.client.visualizations.AnnotatedTimeLine.Options;
import com.greg.gamedesire.client.IGamedesireView;
import com.greg.gamedesire.client.SingleStatistics;
import com.greg.gamedesire.client.views.RankingView;
import com.greg.gamedesire.shared.dto.Match;
import com.mvp4g.client.annotation.Presenter;

@Presenter(view = RankingView.class)
public class RankingPresenter extends SingleStatisticsPresenter<RankingPresenter.IRankingView> {

	public interface IRankingView extends IGamedesireView {
		Panel getTimeLinePanel();
	}
	private boolean apiLoaded;

	@Override
	public void bind() {

		// Create a callback to be called when the visualization API
		// has been loaded.
		Runnable onLoadCallback = new Runnable() {
			public void run() {
				apiLoaded = true;
			}
		};

		VisualizationUtils.loadVisualizationApi(onLoadCallback,
				AnnotatedTimeLine.PACKAGE);
	}

	@Override
	protected void displayStatisticsOfUser(String user) {
		showRankingOfUser(user);		
	}
	
	@Override
	protected SingleStatistics getType() {
		return SingleStatistics.RANKING;
	}

	private Options createOptions() {
		Options options = Options.create();
		options.setOption("displayAnnotations", true);
		options.setOption("fill", Double.parseDouble("10"));
		options.setOption("thickness", Double.parseDouble("2"));
		options.setOption("displayExactValues", true);
		options.setOption("scaleType", "maximized");
		options.setOption("dateFormat", "HH:mm MMMM dd, yyyy");

		return options;
	}

	private void showRankingOfUser(final String user) {
		if(!users.contains(user))
			return;
		if(!apiLoaded)
		{
			Scheduler.get().scheduleDeferred(new ScheduledCommand() {
				
				@Override
				public void execute() {
					showRankingOfUser(user);					
				}
			});
			return;
		}
		
		List<Match> matchesOfPlayer;
		matchesOfPlayer = utility.extractMatchesOfPlayer(matches, user);

		DataTable data = createTable(matchesOfPlayer, user);

		AnnotatedTimeLine lchart = new AnnotatedTimeLine(data,
				createOptions(), "600px", "400px");
		Panel timelinepanel = view.getTimeLinePanel();
		timelinepanel.clear();
		timelinepanel.add(lchart);
	}

	private DataTable createTable(List<Match> matches, String user) {
		DataTable data = DataTable.create();

		data.addColumn(ColumnType.DATE, "Tag");
		data.addColumn(ColumnType.NUMBER, "Ranking");
		data.addColumn(ColumnType.STRING, "Extremwert");
		data.addRows(matches.size());

		double highestRankingValue = 0;
		int highestRankingRow = 0;
		int lowestRankingRow = 0;
		double lowestRankingValue = 10000;
		for (int i = 0; i < matches.size(); i++) {

			Match match = matches.get(i);
			double ranking = getRankingOfUser(user, match);
			if (ranking == 0)
				continue;
			data.setValue(i, 0, match.date);
			data.setValue(i, 1, ranking);
			if (ranking > highestRankingValue) {
				highestRankingValue = ranking;
				highestRankingRow = i;
			}
			if (ranking < lowestRankingValue) {
				lowestRankingValue = ranking;
				lowestRankingRow = i;
			}
		}
		data.setValue(0, 2, "Currently: " + getRankingOfUser(user, matches.get(0)));
		data.setValue(highestRankingRow, 2, "Top Ranking: " + highestRankingValue);
		data.setValue(lowestRankingRow, 2, "Flop Ranking: " + lowestRankingValue);
		

		return data;
	}

	private double getRankingOfUser(String user, Match match) {
		double ranking = (user.equals(match.player1)) ? match.ranking1
				: match.ranking2;
		return ranking;
	}
}
