package com.greg.gamedesire.client.views;

import java.util.HashMap;
import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;
import com.greg.gamedesire.client.presenters.MenuPresenter.IMenuView;
import com.greg.gamedesire.client.resources.MyResources;
import com.greg.gamedesire.client.widget.ListItemWidget;
import com.greg.gamedesire.client.widget.UnorderedListWidget;

/**
 * The menu view implementation.
 * 
 * @author Gregor Riegler
 * 
 */
@Singleton
public class MenuView extends Composite implements IMenuView
{
	private static IMenuViewUiBinder	uiBinder	= GWT.create(IMenuViewUiBinder.class);

	interface IMenuViewUiBinder extends UiBinder<Widget, MenuView>
	{
	}

	@UiField
	UnorderedListWidget								list;
	private final HashMap<String, ListItemWidget>	nameToWidget;


	public MenuView()
	{
		nameToWidget = new HashMap<String, ListItemWidget>();

		initWidget(uiBinder.createAndBindUi(this));

	}


	@Override
	public IsWidget addLink(String name, String token)
	{
		ListItemWidget listItem = nameToWidget.get(name);
		if(listItem == null)
		{
			String linkText = "<span>" + name + "</span>";
			Anchor link = new Anchor(linkText, true);
			link.setHref(token);
			ListItemWidget li = new ListItemWidget(link);
			list.add(li);
			nameToWidget.put(name, li);
			return link;
		}
		return null;
	}


	@Override
	public void removeLink(String name)
	{
		ListItemWidget listItem = nameToWidget.get(name);
		if(listItem != null)
		{
			list.remove(listItem);
			nameToWidget.remove(name);
		}
	}


	@Override
	public void removeAllLinks()
	{
		list.clear();
		nameToWidget.clear();		
	}


	@Override
	public void setActiveLink(IsWidget source)
	{
		if(source == null) 
		{
			return;
		}
		deactivateLinks();
		source.asWidget().setStyleName(MyResources.INSTANCE.css().menuLinkActive(), true);		
	}


	@Override
	public void deactivateLinks()
	{
		Iterator<String> it = nameToWidget.keySet().iterator();
		while(it.hasNext()) {	
			String key = it.next();
			Widget link = nameToWidget.get(key).getWidget();
			link.setStyleName(MyResources.INSTANCE.css().menuLinkActive(), false);	
		}
		
	}

	@Override
	public void reset()
	{
	}
}
