package com.greg.gamedesire.client.views;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.greg.gamedesire.client.presenters.BreaksPresenter.IBreaksView;
import com.greg.gamedesire.client.resources.MyResources;

public class BreaksView extends Composite implements IBreaksView 
{
	private static final int BREAKS_NO = 15;
	private static IUiBinder uiBinder = GWT.create(IUiBinder.class);

	interface IUiBinder extends UiBinder<Widget, BreaksView> {
	}
	
	public int matchesWithBreakNumber;
	
	@UiField Panel breaksPanel;
	@UiField Panel breakAreas;
	@UiField Panel avgBreakArea;
	
	public BreaksView() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void setTopBreaks(List<Integer> breaks) 
	{
		breaksPanel.clear();
		
		int breaksNo;
		if(breaks.size() < BREAKS_NO)
			breaksNo = breaks.size();
		else
			breaksNo = BREAKS_NO;
		
		for(int i=0; i < breaksNo; i++)
		{		
			String breakString = (i+1) + ". <span style=\"float:right\">" + breaks.get(i) + " points</span>";
			HTML breakLabel = new HTML(breakString);
			breakLabel.addStyleName(MyResources.INSTANCE.css().breakLine());
			if(i<=2)
				breakLabel.addStyleName(MyResources.INSTANCE.css().bold());
			breaksPanel.add(breakLabel);
		}		
	}


	@Override
	public void setBreaksMoreThan(Map<Integer, Integer> breaksMoreThan) {
		breakAreas.clear();
		
		Set<Integer> limits = breaksMoreThan.keySet();
		ArrayList<Integer> limitsList = new ArrayList<Integer>(limits);
		Collections.sort(limitsList);
		for (int limit : limitsList) {
			int times = breaksMoreThan.get(limit);
			double percentage = ((double) times) / ((double)matchesWithBreakNumber) * 100;
			String percentageStr = String.valueOf(percentage);
			int length = percentageStr.length();
			percentageStr = String.valueOf(percentage).substring(0, (length < 4) ? length+1 : 4);

			String breakString = "At least " + limit + " points: <span style=\"float:right; font-weight: bold\">" + times + " times (in " + percentageStr + "% of matches)</span>";
			HTML breakLabel = new HTML(breakString);
			breakLabel.addStyleName(MyResources.INSTANCE.css().breakLineAreas());
			breakAreas.add(breakLabel);
		}
		
	}
	@Override
	public void setAverageMaxBreak(float avgMax) {
		avgBreakArea.clear();
		
		String avgMaxBreak = String.valueOf(avgMax);
		int length = avgMaxBreak.length();
		avgMaxBreak = avgMaxBreak.substring(0, (length < 4) ? length+1 : 4);
		
		String breakString = "Average Max Break: <span style=\"float:right; font-weight: bold; text-decoration: underline\">" + avgMaxBreak + "</span>";
		HTML breakLabel = new HTML(breakString);
		breakLabel.addStyleName(MyResources.INSTANCE.css().breakLineAreas());
		avgBreakArea.add(breakLabel);
		
	}

	
	@Override
	public void setMatchesWithBreakNumber(int size) {
		matchesWithBreakNumber = size;
		
	}
	
	@Override
	public void reset() {		
	}


}
