package com.greg.gamedesire.client;

import java.util.Arrays;
import java.util.List;

import com.mvp4g.client.annotation.History;
import com.mvp4g.client.annotation.History.HistoryConverterType;
import com.mvp4g.client.history.HistoryConverter;

@History(type = HistoryConverterType.SIMPLE)
public class SingleUserEventsHistoryConverter implements HistoryConverter<GamedesireEventBus> {


	@Override
	public void convertFromToken(String historyName, String param,
			GamedesireEventBus eventBus) {

		List<String> parts = Arrays.asList(param.split("="));
		String user = "";
		if(parts.size() == 2)
			user = parts.get(1);
		eventBus.dispatch(historyName, user);
		eventBus.selectSingleUser(user);
	}

	public String convertToToken(String eventName, String user) {
		return "user=" + user;
	}

	@Override
	public boolean isCrawlable() {
		return false;
	}

}
