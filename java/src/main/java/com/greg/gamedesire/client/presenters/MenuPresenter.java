package com.greg.gamedesire.client.presenters;

import com.google.gwt.user.client.ui.IsWidget;
import com.greg.gamedesire.client.IGamedesireView;
import com.greg.gamedesire.client.views.MenuView;
import com.mvp4g.client.annotation.Presenter;

@Presenter(view = MenuView.class)
public class MenuPresenter extends WebPresenter<MenuPresenter.IMenuView>
{

	public interface IMenuView extends IGamedesireView
	{
		/**
		 * Adds a link to the main menu.
		 * 
		 * @param name
		 *        name of the link to add
		 * @param token
		 *        the link should refer to
		 */
		IsWidget addLink(String name, String token);


		void removeLink(String string);


		void removeAllLinks();


		void setActiveLink(IsWidget source);


		void deactivateLinks();
	}

	private IsWidget	newsLink;
	private IsWidget	rankingLink;
	private IsWidget	matchesLink;
	private IsWidget 	breaksLink;
	private IsWidget headToHeadLink;

	
	@Override
	public void bind()
	{
		view.removeAllLinks();
		newsLink = view.addLink("Latest", "#" + getTokenGenerator().showLatestMatches());
		rankingLink = view.addLink("Ranking", "#" + getTokenGenerator().showRanking(""));
		matchesLink = view.addLink("Matches", "#" + getTokenGenerator().showMatches(""));
		breaksLink = view.addLink("Breaks", "#" + getTokenGenerator().showBreaks(""));
		headToHeadLink = view.addLink("Head to Head", "#" + getTokenGenerator().showHeadToHead("",""));
	}

	
	public void onShowRanking(String user)
	{
		view.setActiveLink(rankingLink);
	}
	
	public void onShowLatestMatches()
	{
		view.setActiveLink(newsLink);
	}	
	
	public void onShowBreaks(String user)
	{
		view.setActiveLink(breaksLink);
	}
	
	public void onShowHeadToHead(String user1, String user2)
	{
		view.setActiveLink(headToHeadLink);
	}
	
	public void onShowMatches(String user)
	{
		view.setActiveLink(matchesLink);
	}
	
	public void onStart() {}


}
