package com.greg.gamedesire.client.presenters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.visualization.client.AbstractDataTable;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.VisualizationUtils;
import com.google.gwt.visualization.client.visualizations.PieChart;
import com.google.gwt.visualization.client.visualizations.PieChart.Options;
import com.google.inject.Inject;
import com.greg.gamedesire.client.IGamedesireView;
import com.greg.gamedesire.client.views.HeadToHeadView;
import com.greg.gamedesire.shared.ISharedUtility;
import com.greg.gamedesire.shared.dto.Match;
import com.mvp4g.client.annotation.Presenter;

@Presenter(view = HeadToHeadView.class)
public class HeadToHeadPresenter extends
		WebPresenter<HeadToHeadPresenter.IHeadToHeadView> {
	@Inject
	ISharedUtility util;

	private class SelectFavoriteUserChangeHandler implements ChangeHandler {
		@Override
		public void onChange(ChangeEvent event) {
			user1Wanted = selectUser1.getView().getSelectedUser();
			user2Wanted = selectUser2.getView().getSelectedUser();

			eventBus.showHeadToHead(user1Wanted, user2Wanted);
		}
	}

	private class SelectOpponentUserChangeHandler implements ChangeHandler {
		@Override
		public void onChange(ChangeEvent event) {
			user1Wanted = selectUser1.getView().getSelectedUser();
			user2Wanted = selectUser2.getView().getSelectedUser();

			eventBus.showHeadToHead(user1Wanted, user2Wanted);
		}
	}

	public interface IHeadToHeadView extends IGamedesireView {

		void setSelectable1(IsWidget asWidget);

		void setSelectable2(IsWidget asWidget);

		void setPieChart(IsWidget pie);

		void showRankingChanges(float ranking1, float ranking2);

	}

	private List<String> users = Collections.emptyList();
	private List<Match> matches = Collections.emptyList();

	private SelectUserPresenter selectUser1;
	private SelectUserPresenter selectUser2;
	private String user1Wanted = "";
	private String user2Wanted = "";
	private boolean apiLoaded;

	@Override
	public void bind() {
		selectUser1 = eventBus.addHandler(SelectUserPresenter.class);
		selectUser1.addChangeHandler(new SelectFavoriteUserChangeHandler());
		selectUser2 = eventBus.addHandler(SelectUserPresenter.class);
		selectUser2.addChangeHandler(new SelectOpponentUserChangeHandler());

		view.setSelectable1(selectUser1.getView().asWidget());
		view.setSelectable2(selectUser2.getView().asWidget());

		Runnable onLoadCallback = new Runnable() {

			public void run() {
				apiLoaded = true;				
			}
		};

		VisualizationUtils.loadVisualizationApi(onLoadCallback,
				PieChart.PACKAGE);
	}

	public void onMatchesFetched(ArrayList<Match> matches) {
		this.matches = matches;
		initializeBoxes();
	}

	public void onUsersFetched(List<String> users) {
		this.users = users;
		initializeBoxes();
	}

	public void onShowHeadToHead(String user1, String user2) {
		user1Wanted = user1;
		user2Wanted = user2;
		eventBus.changeContent(view.asWidget());

		if (!matches.isEmpty() && !users.isEmpty())
			updateOpponents();
	}

	private void initializeBoxes() {
		if (matches.isEmpty() || users.isEmpty())
			return;

		updateOpponents();
	}

	private void updateOpponents() {
		determineOpponents();

		updateListBoxes();
		
		showRankingChanges();

		showPieChart();
	}

	private void showRankingChanges() {
		List<Match> matchesOfPlayers = util.extractMatchesOfPlayers(matches, user1Wanted, user2Wanted);
		float ranking1 = 0;
		float ranking2 = 0;
		for (Match match : matchesOfPlayers) {
			if(user1Wanted.equals(match.player1))
			{
				ranking1 += match.difference1;
				ranking2 += match.difference2;
			}
			else
			{
				ranking1 += match.difference2;
				ranking2 += match.difference1;
			}
		}
		view.showRankingChanges(ranking1, ranking2);
		
	}

	private void showPieChart() {
		if(!apiLoaded)
		{
			Scheduler.get().scheduleDeferred(new ScheduledCommand() {				
				@Override
				public void execute() {
					showPieChart();				
				}
			});
			return;
		}
		int wins1 = getWins(user1Wanted);
		int wins2 = getWins(user2Wanted);
		int totalWins = wins1 + wins2;
		String user1Legend = user1Wanted + " (" + wins1 + ")";
		String user2Legend = user2Wanted + " (" + wins2 + ")";
		PieChart pie = new PieChart(createTable(user1Legend, wins1, user2Legend, wins2), createOptions(totalWins));
		view.setPieChart(pie);
	}

	private int getWins(String winnerUser) {
		List<Match> matchesOfPlayers = util.extractMatchesOfPlayers(matches, user1Wanted, user2Wanted);
		int i = 0;
		for (Match match : matchesOfPlayers) {
			if(winnerUser.equals(match.winner))
				i++;
		}
		return i;
	}

	private AbstractDataTable createTable(String user1, int wins1, String user2, int wins2) {
		DataTable data = DataTable.create();
		data.addColumn(ColumnType.STRING, "Player");
		data.addColumn(ColumnType.NUMBER, "Number of wins");
		data.addRows(2);
		data.setValue(0, 0, user1);
		data.setValue(0, 1, wins1);
		data.setValue(1, 0, user2);
		data.setValue(1, 1, wins2);
		return data;
	}

	private Options createOptions(int totalWins) {
		Options options = Options.create();
		options.setWidth(430);
		options.setHeight(340);
		options.setBackgroundColor("#EDF7E7");
		options.set3D(true);
		options.setTitle(totalWins + " games");
		options.setLegendBackgroundColor("#EDF7E7");
		return options;
	}

	private void updateListBoxes() {
		List<String> opponentsOfUser1 = util.getOpponentsOfUser(matches,
				user1Wanted);
		selectUser1.setUsers(util.getUsersWithoutUser(users, user2Wanted));
		selectUser2.setUsers(util.getUsersWithoutUser(opponentsOfUser1,
				user1Wanted));
		selectUser1.selectUser(user1Wanted);
		selectUser2.selectUser(user2Wanted);
	}

	private void determineOpponents() {
		if (!users.contains(user1Wanted)) {
			if (!users.contains(user2Wanted)) {
				user1Wanted = users.get(0);
				user2Wanted = util.getArbitraryOpponent(matches, user1Wanted);
			} else {
				user1Wanted = user2Wanted;
				user2Wanted = util.getArbitraryOpponent(matches, user1Wanted);
			}
		} else if (!util.getOpponentsOfUser(matches, user1Wanted).contains(
				user2Wanted))
			user2Wanted = util.getArbitraryOpponent(matches, user1Wanted);

	}

}
