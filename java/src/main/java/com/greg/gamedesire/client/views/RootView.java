package com.greg.gamedesire.client.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.greg.gamedesire.client.presenters.RootPresenter.IRootView;
import com.greg.gamedesire.client.resources.MyResources;

public class RootView extends Composite implements IRootView {
	private static IUiBinder	uiBinder	= GWT.create(IUiBinder.class);

	interface IUiBinder extends UiBinder<Widget, RootView>
	{
	}
	
	@Inject 
	MenuView menuView;
	
	@UiField DockLayoutPanel mainPanel;
	@UiField FlowPanel mainMenu;
	@UiField FlowPanel content;
	

	public RootView() {
		initWidget(uiBinder.createAndBindUi(this));
		MyResources.INSTANCE.css().ensureInjected();
	}
	

	@Override
	public void changeContent(IsWidget widget) {
		content.clear();
		content.add(widget);		
	}


	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void setStaticViews() {
		mainMenu.add(menuView);		
	}


}
