package com.greg.gamedesire.client.presenters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.greg.gamedesire.client.IGamedesireView;
import com.greg.gamedesire.client.SingleStatistics;
import com.greg.gamedesire.client.views.BreaksView;
import com.greg.gamedesire.shared.dto.Match;
import com.mvp4g.client.annotation.Presenter;

@Presenter(view = BreaksView.class)
public class BreaksPresenter extends
SingleStatisticsPresenter<BreaksPresenter.IBreaksView> 
{ 
	
	public interface IBreaksView extends IGamedesireView 
	{
		void setTopBreaks(List<Integer> breaks);

		void setBreaksMoreThan(Map<Integer, Integer> breaksMoreThan);

		void setMatchesWithBreakNumber(int size);
		
		void setAverageMaxBreak(float avgMax);
	}	

	@Override
	protected void displayStatisticsOfUser(String user) {
		int matchesWithPositiveBreak = filterMatchesWithPositiveBreak(matches, user).size();
		view.setMatchesWithBreakNumber(matchesWithPositiveBreak);
		
		List<Integer> breaks = new ArrayList<Integer>();
		for (Match match : matches) {
			if(match.player1.equals(user))
				breaks.add(match.maxBreak1);
			else if(match.player2.equals(user))
				breaks.add(match.maxBreak2);
		}
		
		Collections.sort(breaks);
		Collections.reverse(breaks);
		view.setTopBreaks(breaks);
		
		Map<Integer, Integer> breaksMoreThan = new HashMap<Integer, Integer>() {{
			put(30, 0);
			put(50, 0);
			put(75, 0);
			put(100, 0);
		}};
		
		float avgBreak = 0;
		for (int singleBreak : breaks) {
			avgBreak = avgBreak + singleBreak;
			
			Set<Integer> limits = breaksMoreThan.keySet();
			for (int limit: limits) {
				if(singleBreak >= limit) {
					breaksMoreThan.put(limit, breaksMoreThan.get(limit)+1);
				}
			}
		}
		
		avgBreak = avgBreak / matchesWithPositiveBreak;		
		
		view.setBreaksMoreThan(breaksMoreThan);
		
		view.setAverageMaxBreak(avgBreak);
	}

	private List<Match> filterMatchesWithPositiveBreak(
			List<Match> matches, String user) {
		List<Match> filteredMatches = new ArrayList<Match>();
		for (Match match : matches) {
			if((match.player1.equalsIgnoreCase(user) && match.maxBreak1>0) || (match.player2.equalsIgnoreCase(user) && match.maxBreak2>0)) {
				filteredMatches.add(match);
			}
		}
		return filteredMatches;
	}

	@Override
	protected SingleStatistics getType() {
		return SingleStatistics.BREAKS;
	}


}
