package com.greg.gamedesire.client.eventHandler;

import java.util.Arrays;
import java.util.List;

import net.customware.gwt.dispatch.client.DispatchAsync;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.greg.gamedesire.client.GamedesireEventBus;
import com.greg.gamedesire.shared.dispatch.action.GetAllMatches;
import com.greg.gamedesire.shared.dispatch.result.GetAllMatchesResult;
import com.mvp4g.client.annotation.EventHandler;
import com.mvp4g.client.event.BaseEventHandler;


@Singleton
@EventHandler
public class StartHandler extends BaseEventHandler<GamedesireEventBus>
{
	@Inject 
	DispatchAsync dispatch;
	
	public void onStart()
	{
		fetchMatches();
		
		fetchUsers();		
		
	}

	private void fetchMatches() {
		dispatch.execute(new GetAllMatches(), new AsyncCallback<GetAllMatchesResult>() {

			@Override
			public void onFailure(Throwable error) {
				
				
			}

			@Override
			public void onSuccess(GetAllMatchesResult result) {
				eventBus.matchesFetched(result.getMatches());
				
			}
			
		});
	}
	
	private void fetchUsers() {
		try {
			new RequestBuilder(RequestBuilder.GET, "../favorites.txt").sendRequest("", new RequestCallback() {

						public void onResponseReceived(Request request,
								Response response) {
							List<String> favorites = Arrays.asList(response.getText().split(","));
							
							eventBus.usersFetched(favorites);
							
						}

						public void onError(Request request, Throwable exception) {
							exception.printStackTrace();
						}

					});
		} catch (RequestException e) {
			// fire event
		}
		
	}

}
