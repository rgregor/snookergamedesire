package com.greg.gamedesire.client.views;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.greg.gamedesire.client.presenters.MatchesPresenter.IMatchesView;
import com.greg.gamedesire.shared.ISharedUtility;
import com.greg.gamedesire.shared.dto.Match;

public class MatchesView extends Composite implements IMatchesView {
	private static IUiBinder uiBinder = GWT.create(IUiBinder.class);

	interface IUiBinder extends UiBinder<Widget, MatchesView> {
	}
	
	@Inject
	ISharedUtility utility;

	@UiField FlexTable resultTable = new FlexTable();

	public MatchesView() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void reset() {	}
	
	private String matchToString(Match match) {
		String loser = utility.getLoser(match);		
		int maxBreak = utility.getMaxBreak(match);
		
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd.MMMM - HH:mm:");

		return dateFormat.format(match.date) + " " + match.winner + " wins against " + loser + ", the highest break was " + maxBreak;
	}

	@Override
	public void showMatches(List<Match> matchesOfPlayer) {
		StringBuilder builder = new StringBuilder();
		builder.append(matchesOfPlayer.size()).append(" Matches found<br/><br/>");
		for (Match match : matchesOfPlayer) {
			builder.append(matchToString(match)).append("<br/>");
		}
		resultTable.clear();
		resultTable.setWidget(0, 0, new HTMLPanel(builder.toString()));	
	}

}
