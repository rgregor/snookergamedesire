package com.greg.gamedesire.client.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.greg.gamedesire.client.presenters.RankingPresenter.IRankingView;

public class RankingView extends Composite implements IRankingView {
	private static IUiBinder uiBinder = GWT.create(IUiBinder.class);

	interface IUiBinder extends UiBinder<Widget, RankingView> {
	}

	@UiField
	Panel timeLinePanel;

	@Inject
	public RankingView() {
		initWidget(uiBinder.createAndBindUi(this));

	}

	public Panel getTimeLinePanel() {
		return timeLinePanel;
	}

	@Override
	public void reset() {
	}
}
