package com.greg.gamedesire.client;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.mvp4g.client.annotation.History;
import com.mvp4g.client.annotation.History.HistoryConverterType;
import com.mvp4g.client.history.HistoryConverter;

@History(type = HistoryConverterType.SIMPLE)
public class HeadToHeadHistoryConverter implements HistoryConverter<GamedesireEventBus> {

	@Override
	public void convertFromToken(String historyName, String param,
			GamedesireEventBus eventBus) 
	{
		String userString = "", user1 = "", user2 = "";
		List<String> parts = Collections.emptyList();
		if(param != null)
			parts = Arrays.asList(param.split("="));
		if(parts.size() == 2)
			userString = parts.get(1);
		List<String> users = Arrays.asList(userString.split(";"));
		if(users.size() == 2)
		{
			user1 = users.get(0);
			user2 = users.get(1);
		}
		eventBus.dispatch(historyName, user1, user2);		
	}
	
	public String convertToToken(String eventName, String user1, String user2) {
		return "users=" + user1 + ";" +user2;
	}

	@Override
	public boolean isCrawlable() {
		return false;
	}

}
