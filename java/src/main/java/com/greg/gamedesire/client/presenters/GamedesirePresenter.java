package com.greg.gamedesire.client.presenters;

import net.customware.gwt.dispatch.client.DispatchAsync;

import com.google.inject.Inject;
import com.greg.gamedesire.client.IGamedesireView;
import com.mvp4g.client.event.EventBus;
import com.mvp4g.client.presenter.BasePresenter;

public abstract class GamedesirePresenter <S extends IGamedesireView, T extends EventBus> extends BasePresenter<S, T> 
{
	@Inject
	protected DispatchAsync	dispatch;

}
