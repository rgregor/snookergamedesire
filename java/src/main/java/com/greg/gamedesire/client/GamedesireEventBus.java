package com.greg.gamedesire.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;
import com.greg.gamedesire.client.eventHandler.StartHandler;
import com.greg.gamedesire.client.gin.WebGinModule;
import com.greg.gamedesire.client.presenters.BreaksPresenter;
import com.greg.gamedesire.client.presenters.LatestMatchesPresenter;
import com.greg.gamedesire.client.presenters.MatchesPresenter;
import com.greg.gamedesire.client.presenters.MenuPresenter;
import com.greg.gamedesire.client.presenters.RankingPresenter;
import com.greg.gamedesire.client.presenters.RootPresenter;
import com.greg.gamedesire.client.presenters.SelectUserPresenter;
import com.greg.gamedesire.client.presenters.HeadToHeadPresenter;
import com.greg.gamedesire.client.presenters.SingleUserPresenter;
import com.greg.gamedesire.client.views.RootView;
import com.greg.gamedesire.shared.dto.Match;
import com.mvp4g.client.annotation.Event;
import com.mvp4g.client.annotation.Events;
import com.mvp4g.client.annotation.InitHistory;
import com.mvp4g.client.annotation.Start;
import com.mvp4g.client.event.EventBusWithLookup;

@Events(startView = RootView.class, ginModules = WebGinModule.class, historyOnStart = true)
public interface GamedesireEventBus extends EventBusWithLookup {
	
	@Start
	@Event(handlers = {StartHandler.class, MenuPresenter.class})
	public void start();
	
	
	/** START DATA-FETCHED-EVENTS **/
	@Event(handlers = {HeadToHeadPresenter.class, LatestMatchesPresenter.class, RankingPresenter.class, MatchesPresenter.class, BreaksPresenter.class})
	public void matchesFetched(ArrayList<Match> matches);
	
	@Event(handlers = {HeadToHeadPresenter.class, SingleUserPresenter.class, RankingPresenter.class, MatchesPresenter.class, BreaksPresenter.class} )
	public void usersFetched(List<String> favorites);
	/** END DATA-FETCHED-EVENTS **/
	
	
	/** START SHOW-X EVENTS **/
	@Event(handlers = {SingleUserPresenter.class, MenuPresenter.class,
			RankingPresenter.class, MatchesPresenter.class, BreaksPresenter.class}, 
			historyConverter = SingleUserEventsHistoryConverter.class, name = "ranking")
	public String showRanking(String user);	

	@Event(handlers = {SingleUserPresenter.class, MenuPresenter.class,
			MatchesPresenter.class, RankingPresenter.class, BreaksPresenter.class}, 
			historyConverter = SingleUserEventsHistoryConverter.class, name = "matches")
	public String showMatches(String user);
	
	@Event(handlers = {SingleUserPresenter.class, MenuPresenter.class,
			MatchesPresenter.class, RankingPresenter.class, BreaksPresenter.class}, 
			historyConverter = SingleUserEventsHistoryConverter.class, name = "breaks")
	public String showBreaks(String user);
	
	@Event(handlers = {HeadToHeadPresenter.class, MenuPresenter.class}, historyConverter = HeadToHeadHistoryConverter.class, name="headToHead")
	public String showHeadToHead(String user1, String user2);
	
	
	
	@InitHistory
	@Event(handlers = {MenuPresenter.class,
			LatestMatchesPresenter.class}, historyConverter = GamedesireHistoryConverter.class, name = "latestMatches")
	public String showLatestMatches();
	/** END SHOW-X EVENTS **/
	
	
	/** START CHANGING-WIDGETS EVENTS **/	
	@Event(handlers = SingleUserPresenter.class)
	public void setSingleUserStatistics(IsWidget widget);
	
	@Event(handlers = RootPresenter.class)
	public void changeContent(IsWidget widget);
	/** END CHANGING-WIDGETS EVENTS **/	
	
	
	@Event(handlers = {SingleUserPresenter.class})
	public void selectSingleUser(String user);	

}
