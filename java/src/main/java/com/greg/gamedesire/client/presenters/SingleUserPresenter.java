package com.greg.gamedesire.client.presenters;

import java.util.List;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.IsWidget;
import com.greg.gamedesire.client.IGamedesireView;
import com.greg.gamedesire.client.SingleStatistics;
import com.greg.gamedesire.client.views.SingleUserView;
import com.mvp4g.client.annotation.Presenter;

@Presenter(view = SingleUserView.class)
public class SingleUserPresenter extends WebPresenter<SingleUserPresenter.ISingleUserView> 
{
	public interface ISingleUserView extends IGamedesireView {
		void setSingleUserStatistics(IsWidget widget);

		void setUserSelectable(IsWidget widget);
	}

	private SingleStatistics activeStatistics;
	private SelectUserPresenter selectUser;
	private List<String> users;
	
	@Override
	public void bind() {
		selectUser = eventBus.addHandler(SelectUserPresenter.class, false);
		selectUser.addChangeHandler(new SelectUserChangeHandler());
		selectUser.bind();
		view.setUserSelectable(selectUser.getView().asWidget());
	}
	
	public void onSetSingleUserStatistics(IsWidget widget)
	{
		view.setSingleUserStatistics(widget);
		eventBus.changeContent(view.asWidget());
	}
	
	public void onSelectSingleUser(String user)
	{
		selectUser.selectUser(user);
	}
	
	public void onUsersFetched(List<String> users)
	{
		this.users = users;
		selectUser.setUsers(users);
	}
	
	public void onShowMatches(String user)
	{
		activeStatistics = SingleStatistics.MATCHES;		
	}
	
	public void onShowRanking(String user)
	{
		activeStatistics = SingleStatistics.RANKING;	
	}
	
	public void onShowBreaks(String user)
	{
		activeStatistics = SingleStatistics.BREAKS;	
	}
	
	private final class SelectUserChangeHandler implements ChangeHandler {

		
		@Override
		public void onChange(ChangeEvent event) {
			String user = selectUser.getView().getSelectedUser();
			switch(activeStatistics)
			{
			case MATCHES:
				eventBus.showMatches(user);
				break;
			case RANKING:
				eventBus.showRanking(user);
				break;
			case BREAKS:
				eventBus.showBreaks(user);
				break;
			default:
				throw new RuntimeException("No active statistics field in SelectUserPresenter!");
			}
		}
	}

}
