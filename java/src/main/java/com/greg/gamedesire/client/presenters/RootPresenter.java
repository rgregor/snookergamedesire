package com.greg.gamedesire.client.presenters;

import com.google.gwt.user.client.ui.IsWidget;
import com.greg.gamedesire.client.IGamedesireView;
import com.greg.gamedesire.client.presenters.RootPresenter.IRootView;
import com.greg.gamedesire.client.views.RootView;
import com.mvp4g.client.annotation.Presenter;

@Presenter(view = RootView.class)
public class RootPresenter extends WebPresenter<IRootView> {

	public interface IRootView extends IGamedesireView {
		void changeContent(IsWidget widget);
		void setStaticViews();
	}
	
	public void onChangeContent(IsWidget widget)
	{
		view.changeContent(widget);
	}
	
	@Override
	public void bind()
	{
		view.setStaticViews();
	}

}
