package com.greg.gamedesire.client.eventHandler;

import net.customware.gwt.dispatch.client.DispatchAsync;
import net.customware.gwt.dispatch.client.ExceptionHandler;
import net.customware.gwt.dispatch.client.standard.StandardDispatchService;
import net.customware.gwt.dispatch.client.standard.StandardDispatchServiceAsync;
import net.customware.gwt.dispatch.shared.Action;
import net.customware.gwt.dispatch.shared.Result;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.inject.Singleton;
import com.greg.gamedesire.client.GamedesireEventBus;
import com.greg.gamedesire.client.device.RPCUrlProvider;
import com.mvp4g.client.annotation.EventHandler;
import com.mvp4g.client.event.BaseEventHandler;

/**
 * Implementation of the gwt-dispatch DispatchAsync, additionally providing ajax loader gif display 
 * and setting of the remote service url to normal gwt-dispatch command pattern.
 * 
 * 
 */
@EventHandler
@Singleton
public class GenericDispatchAsync extends BaseEventHandler<GamedesireEventBus> implements DispatchAsync
{
	private static final StandardDispatchServiceAsync	REALSERVICE	= GWT.create(StandardDispatchService.class);
	private static final RPCUrlProvider				URLSERVICE	= GWT.create(RPCUrlProvider.class);

	private ExceptionHandler						exceptionHandler;

	/**
	 * .ctor
	 * Necessary for GIN
	 */
	public GenericDispatchAsync()
	{
		super();
	}
	
	public void onStart()
	{
		
	}

	protected <A extends Action<R>, R extends Result> void onFailure(A action, Throwable caught, final AsyncCallback<R> callback)
	{
		if(exceptionHandler != null && exceptionHandler.onFailure(caught) == ExceptionHandler.Status.STOP)
		{
			return;
		}
		//eventBus.hideLoadingDialog();

		callback.onFailure(caught);
	}

	protected <A extends Action<R>, R extends Result> void onSuccess(A action, R result, final AsyncCallback<R> callback)
	{
		callback.onSuccess(result);
	}


	public <A extends Action<R>, R extends Result> void execute(final A action, final AsyncCallback<R> callback)
	{
		//eventBus.startAjaxCall();

		((ServiceDefTarget) REALSERVICE).setServiceEntryPoint(URLSERVICE.getRPCPrefix() + "/dispatch");

		REALSERVICE.execute(action, new AsyncCallback<Result>()
		{
			public void onFailure(Throwable caught)
			{
				//eventBus.stopAjaxCall();
				GenericDispatchAsync.this.onFailure(action, caught, callback);
			}


			public void onSuccess(Result result)
			{
				//eventBus.stopAjaxCall();
				GenericDispatchAsync.this.onSuccess(action, (R) result, callback);
			}
		});
	}


}
