package com.greg.gamedesire.client.presenters;

import com.greg.gamedesire.client.GamedesireEventBus;
import com.greg.gamedesire.client.IGamedesireView;

public abstract class WebPresenter<S extends IGamedesireView> 
extends GamedesirePresenter<S, GamedesireEventBus> 
{

}
