package com.greg.gamedesire.client.widget;



import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * GWT widget that corresponds to a HTML ul tag.
 * Used in combination with {@link ListItemWidget} to write
 * a best-practice-structured menu in form 
 * <pre>
 * &lt;ul&gt;
 * 	&lt;li&gt;first li&lt;/li&gt;
 * 	&lt;li&gt;second li&lt;/li&gt;
 * &lt;/ul&gt;</pre>
 * More info: http://turbomanage.wordpress.com/2010/02/11/writing-plain-html-in-gwt/
 * 
 * @author Gregor Riegler
 *
 */
public class UnorderedListWidget extends ComplexPanel
{
	public UnorderedListWidget()
	{
		setElement(Document.get().createULElement());
	}

	/**
	 * Sets the id attribute of this ul tag
	 * 
	 * @param id the id of the ul
	 */
	public void setHtmlId(String id)
	{
		getElement().setId(id);
	}

	public void add(Widget w)
	{
		// ComplexPanel requires the two-arg add() method
		super.add(w, getElement());
	}
}
