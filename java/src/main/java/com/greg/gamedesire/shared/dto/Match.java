package com.greg.gamedesire.shared.dto;
import java.io.Serializable;
import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.IsSerializable;
import com.googlecode.objectify.annotation.Cached;
import com.greg.gamedesire.shared.EndType;

import javax.persistence.Id;


public class Match implements Serializable, IsSerializable, Comparable<Match> {

	private static final long serialVersionUID = 5849045288404737503L;
	public Long id;
	public Date date;
	public int duration;
	public String player1;
	public String player2;
	public String winner;
	public double ranking1;
	public double ranking2;
	public double difference1;
	public double difference2;
	public boolean rated;
	public EndType endType;
	public int maxBreak1;
	public int maxBreak2;
	
	public Match() { }
	
	public Match(String winner) {
		this.winner = winner;
	}
	
	public Match(Match match) {
		this.date = match.date;
		this.duration = match.duration;
		this.player1 = match.player1;
		this.player2 = match.player2;
		this.winner = match.winner;
		this.ranking1 = match.ranking1;
		this.ranking2 = match.ranking2;
		this.difference1 = match.difference1;
		this.difference2 = match.difference2;
		this.rated = match.rated;
		this.endType = match.endType;
		this.maxBreak1 = match.maxBreak1;
		this.maxBreak2 = match.maxBreak2;
	}
	
	@Override
	public String toString() {
		String loser = (winner.equals(player1)) ? player2 : player1;
		int maxBreak = (maxBreak1 > maxBreak2) ? maxBreak1 : maxBreak2;
		return "At " + date + " " + winner + " wins against " + loser + ", the highest break was " + maxBreak + "<br>";
	}
	
	

	public String toStringDeluxe() {
		return "Match [id=" + id + ", date=" + date + ", duration=" + duration
				+ ", player1=" + player1 + ", player2=" + player2 + ", winner="
				+ winner + ", ranking1=" + ranking1 + ", ranking2=" + ranking2
				+ ", difference1=" + difference1 + ", difference2="
				+ difference2 + ", rated=" + rated + ", endType=" + endType
				+ ", maxBreak1=" + maxBreak1 + ", maxBreak2=" + maxBreak2 + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Match)) return false;
		Match match = (Match) obj;
		if(!(duration == match.duration)) return false;
		if(!(player1.equals(match.player1) || player1.equals(match.player2))) return false;
		if(!(player2.equals(match.player2) || player2.equals(match.player1))) return false;
		if(!(winner.equals(match.winner))) return false;
		if(!(ranking1 == match.ranking1 || ranking1 == match.ranking2)) return false;
		if(!(ranking2 == match.ranking2 || ranking2 == match.ranking1)) return false;
		if(!(difference1 == match.difference1 || difference1 == match.difference2)) return false;
		if(!(difference2 == match.difference2 || difference2 == match.difference1)) return false;
		if(!(rated == match.rated)) return false;
		if(!(endType.equals(match.endType))) return false;
		if(!(maxBreak1 == match.maxBreak1 || maxBreak1 == match.maxBreak2)) return false;
		if(!(maxBreak2 == match.maxBreak2 || maxBreak2 == match.maxBreak1)) return false;
		return true;		
	}

	public int compareTo(Match other) {
		if(date.before(other.date)) return -1;
		if(date.after(other.date)) return 1;
		return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		long temp;
		result = prime * result + (new Double(difference1+difference2)).intValue();
		result = prime * result + duration;
		result = prime * result + ((endType == null) ? 0 : endType.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + maxBreak1 + maxBreak2;
		result = prime * result + (new Double(ranking1+ranking2)).intValue();
		result = prime * result + (rated ? 1231 : 1237);
		result = prime * result + ((winner == null) ? 0 : winner.hashCode());
		return result;
	}
}
