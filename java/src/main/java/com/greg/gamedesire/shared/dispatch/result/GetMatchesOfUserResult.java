package com.greg.gamedesire.shared.dispatch.result;

import java.util.ArrayList;

import net.customware.gwt.dispatch.shared.Result;

import com.greg.gamedesire.shared.dto.Match;

public class GetMatchesOfUserResult implements Result 
{
	private ArrayList<Match> matches;
	
	public GetMatchesOfUserResult() {}

	public GetMatchesOfUserResult(ArrayList<Match> matches) 
	{
		this.matches = matches;
	}	
	
}