package com.greg.gamedesire.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

import net.customware.gwt.dispatch.shared.ActionException;

public class GamedesireActionException extends RuntimeException implements IsSerializable
{
	public GamedesireActionException() 
	{
		
	}
	
	public GamedesireActionException(String message)
	{
		super(message);
	}
	
	public GamedesireActionException(Exception e)
	{
		super(e);
	}


	public GamedesireActionException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
