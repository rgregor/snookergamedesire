package com.greg.gamedesire.shared;

import java.util.List;
import java.util.Set;

import com.greg.gamedesire.shared.dto.Match;

public interface ISharedUtility {

	public abstract List<Match> extractMatchesOfPlayer(List<Match> allMatches, String user);
	public abstract boolean hasValue(String stringToTest);
	public abstract int getMaxBreak(Match match);
	public abstract String getLoser(Match match);
	public abstract List<Match> extractMatchesOfPlayers(List<Match> allMatches, String user1,
			String user2);
	List<String> getOpponentsOfUser(List<Match> matches, String user);
	String getArbitraryOpponent(List<Match> matches, String user);
	List<String> getUsersWithoutUser(List<String> users, String user);
	
}
