package com.greg.gamedesire.shared.dto;

import java.util.List;

public interface IEntitiesContainer<T> {
	public List<T> getEntities();
	void addEntities(List<T> matchEntities);
	
	public int getMaxNumberEntities();

}
