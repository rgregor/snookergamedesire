package com.greg.gamedesire.shared.dispatch.action;

import net.customware.gwt.dispatch.shared.Action;
import com.greg.gamedesire.shared.dispatch.result.GetMatchesOfUserResult;

public class GetMatchesOfUser implements Action<GetMatchesOfUserResult>
{
	private String username;

	public GetMatchesOfUser(){}
	
	public GetMatchesOfUser(String username) 
	{
		this.username = username;
	}

}
