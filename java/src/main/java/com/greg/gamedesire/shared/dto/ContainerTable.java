package com.greg.gamedesire.shared.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Id;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.googlecode.objectify.annotation.Cached;

public class ContainerTable implements Serializable, IsSerializable {
	private static final long serialVersionUID = -3299124971626654479L;
	
	@Id
	public Long id;
	
	private List<Long> containers = new ArrayList<Long>();
	
	public ContainerTable() {}

	public List<Long> getContainers() {
		return containers;
	}
	
	public void addContainer(Long container) {
		containers.add(container);
	}

	public void setContainers(List<Long> containers) {
		this.containers = containers;
	}

	
}
