package com.greg.gamedesire.shared.dispatch.result;

import java.util.ArrayList;
import java.util.List;

import net.customware.gwt.dispatch.shared.Result;

import com.greg.gamedesire.shared.dto.Match;

public class GetAllMatchesResult implements Result
{
	private ArrayList<Match> matches;
	
	public GetAllMatchesResult() {}
	
	public GetAllMatchesResult(List<Match> matches)
	{
		this.matches = new ArrayList<Match>(matches);
	}

	public ArrayList<Match> getMatches() {
		return matches;
	}

}
