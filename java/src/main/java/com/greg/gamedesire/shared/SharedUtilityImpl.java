package com.greg.gamedesire.shared;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.greg.gamedesire.shared.dto.Match;

public class SharedUtilityImpl implements ISharedUtility {
	/**
	 * Get those matches of <code>allMatches</code> in which
	 * <code>user</code> participated
	 */
	@Override
	public List<Match> extractMatchesOfPlayer(List<Match> allMatches, String user) {
		List<Match> matches = new ArrayList<Match>();
		for (Match match : allMatches) {
			if(match.player1.equals(user) || match.player2.equals(user)) 
				matches.add(match);
		}
		return matches;	
	}
	
	@Override
	public List<String> getOpponentsOfUser(List<Match> matches, String user)
	{
		matches = extractMatchesOfPlayer(matches, user);
		Set<String> opponents = new HashSet<String>();
		for (Match match : matches) {
			opponents.add(getOpponent(user, match));
		}		
		List<String> listOfOpponents = new ArrayList<String>(opponents);
		Collections.sort(listOfOpponents, new Comparator<String>() 
			{
				@Override
				public int compare(String arg0, String arg1) {
					return arg0.compareToIgnoreCase(arg1);
				} 
		});
		return listOfOpponents;
		
	}
	
	@Override
	public String getArbitraryOpponent(List<Match> matches, String user) {
		List<String> opponentsOfUser = getOpponentsOfUser(matches, user);
		if(opponentsOfUser.size() > 0)
			return opponentsOfUser.get(0);
		else
			return "";
	}
	
	@Override
	public List<String> getUsersWithoutUser(List<String> users, String user) {
		List<String> usersOfListBox = new ArrayList<String>(users);
		usersOfListBox.remove(user);
		return usersOfListBox;
	}	

	private String getOpponent(String user, Match match) {
		if(user.equals(match.player1))
			return match.player2;
		else
			return match.player1;
	}
	
	@Override
	public List<Match> extractMatchesOfPlayers(List<Match> allMatches, String user1, String user2) {
		List<Match> matches = new ArrayList<Match>();
		for (Match match : allMatches) {
			boolean bothUsersPlayed = (match.player1.equals(user1) && match.player2.equals(user2)) || 
					(match.player2.equals(user1) && match.player1.equals(user2)); 
			if(bothUsersPlayed)
				matches.add(match);
		}
		return matches;	
	}

	@Override
	public boolean hasValue(String stringToTest) {
		if(stringToTest == null || stringToTest.isEmpty())
			return false;
		else
			return true;
	}
	
	@Override
	public int getMaxBreak(Match match) {
		int maxBreak;
		if(match.maxBreak1 > match.maxBreak2)
			maxBreak = match.maxBreak1;
		else
			maxBreak = match.maxBreak2;
		return maxBreak;
	}
	
	@Override
	public String getLoser(Match match) {
		String loser;
		if(match.winner.equals(match.player1)) 
			loser = match.player2;
		else
			loser = match.player1;
		return loser;
	}

}
