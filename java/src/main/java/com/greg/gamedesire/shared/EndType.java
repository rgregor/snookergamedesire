package com.greg.gamedesire.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum EndType implements IsSerializable {
	DISCONNECT("Disconnect", "DIS"), RESIGN("Resign","RES"), END("Ende","FIN"), TIMEOUT("TimeOut","TimeOut"), UNDEFINED("","");
	
	String typeNameGer;
	String typeNameEng;
	
	EndType(String typeGer, String typeEng) {
		this.typeNameGer = typeGer;
		this.typeNameEng = typeEng;
	}
	
	public static EndType asEnumType(String typeAsString) {
		EndType matchingType=null;
		for (EndType type : EndType.values()) {
			if(type.typeNameGer.equals(typeAsString) || type.typeNameEng.equals(typeAsString)) {
				matchingType = type;
				break;
			}
		}
		if(matchingType != null) {
			return matchingType;
		}
		else {
			return EndType.UNDEFINED;
		}
		//throw new IllegalArgumentException("There is no matching type for the given String " + typeAsString);
		
	}
}
