package com.greg.gamedesire.shared.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Embedded;
import javax.persistence.Id;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.googlecode.objectify.annotation.Cached;

public class MatchesContainerImpl implements Serializable, IsSerializable, IEntitiesContainer<Match> {
	private static final long serialVersionUID = -1450630436967685427L;
	
	@Id
	public Long id;
	
	@Embedded
	public List<Match> matches = new ArrayList<Match>();

	@Override
	public List<Match> getEntities() {
		return matches;
	}

	@Override
	public void addEntities(List<Match> matchEntities) {
		matches.addAll(matchEntities);
		
	}

	@Override
	public int getMaxNumberEntities() {
		return 100;
	}

}
