(ns com.greg.gamedesire.server.impl.core
  (:use [clojure.data.json :only (read-json json-str)]
       ;; [incanter core charts stats datasets]
        )
  (:require [clojure.java.io :as io]
            [clj-time.format :as timef]
            [clj-time.core :as time]
            [clj-time.coerce :as coerce])
  (:import [java.util Locale])
  (:gen-class :name com.greg.gamedesire.server.impl.MatchesFilterImpl
  :implements [com.greg.gamedesire.server.IMatchesFilter])
  )


(declare adjust-date get-clj-from-json-file write-clj-to-file read-from-file-safely date-with-duration sort-matches sorted-matches-of has-won? player-value ranking max-break difference matches-won-in-percent match-series? compute-ranking-lines get-subsequent-matches merge-lines-if-necessary get-matches-lines get-matches-line get-matches-with-avg-maxbreak my-clj)

(defn get-clj-from-json-file [filename]
  """Returns a Clojure datastructure built from the json content of 
    the file designated by the path argument"""
  (let [builder (StringBuilder.)]
    (doseq [line (with-open [rdr (io/reader filename)]
                   (doall (line-seq rdr)))] 
      (.append builder line))
    (map adjust-date (-> builder .toString read-json))))


(defn read-from-file-safely [filename]
  (with-open
    [r (java.io.PushbackReader.
         (clojure.java.io/reader filename))]
    (binding [*read-eval* false]
      (read r))))

(def my-json-file "/home/greg/workspace/application/java/src/main/webapp/matches2")

;; (Un)comment this for production/dev
(def my-clj (get-clj-from-json-file my-json-file))

(def sniper "TheSniper.")
(def moyli "vienna_moyli")
(def pro "der_pro")
(def pausteria "pausteria")
(def ma "die_ma")
(def balanced "Balanced.")
(def jochan "jochan.riegl")
(def momsen "Momsen76")
(def players [sniper moyli pro pausteria balanced jochan momsen])
(def date-formatter (timef/with-locale (timef/formatter "MMM d, yyyy h:mm:ss aaa") (. Locale US)))

(defn -filterMatches [this matches-json drop-matches players]
  (let [matches (->> (read-json matches-json) (map adjust-date))]
    (json-str (map #(assoc % :date (timef/unparse date-formatter (:date %))) (drop drop-matches (distinct (sort-matches (flatten (for [player players] (get-matches-line player (get-matches-lines player matches)))))))))))

(defn- date-with-duration [match]
  (-> match :date coerce/to-long (+ ,,, (-> match :duration (* ,,, 1000))) coerce/from-long))

(defn- sort-matches [matches & order]
  """Sorts the matches ascending or descending by date.
    The order is controlled by the order argument being :asc or :desc"""
  (let [order-fn (if (and order (= (first order) :desc)) #(time/after? % %2) #(time/before? % %2))]
    (sort (fn [x y] (order-fn (date-with-duration x) (date-with-duration y))) matches)))

(def sorted-matches-of
  (memoize 
    (fn [player matches]
      (let [as-player1 (get (group-by :player1 matches) player)
            as-player2 (get (group-by :player2 matches) player)]		
        (sort-matches (into as-player2 as-player1))))))

(defn- has-won? [player match]
  (= player (:winner match)))

(defn- player-value [player match value-player-1 value-player-2 & default]
  (if (= player (:player1 match)) value-player-1
    (if (= player (:player2 match)) value-player-2 default)))

(defn- ranking [player match]
  (player-value player match (:ranking1 match) (:ranking2 match)))

(defn- max-break [player match]
  (player-value player match (:maxBreak1 match) (:maxBreak2 match)))

(defn- difference [player match]
  (player-value player match (:difference1 match) (:difference2 match)))

(defn- matches-won-in-percent [player matches]
  (let [player-matches (get-matches-line player (get-matches-lines player matches))
        matches-won (filter (partial has-won? player) player-matches)]
    (float (/ (count matches-won) (count player-matches)))))

(defn- match-series? [player match1 match2]
  (let [ranking-match2 (ranking player match2)
        ranking-match1 (ranking player match1)
        difference-match2 (difference player match2)
        adjacent (= (format "%.2f" (- ranking-match2  difference-match2))
                    (format "%.2f" ranking-match1))]
    adjacent))

(defn- compute-ranking-lines [player matches]
  (let [sorted-matches (sorted-matches-of player matches)]
    (loop [remaining (seq sorted-matches)
           ranking-lines []
           index 0]
      (if (seq remaining)					
        (let [current-match (first remaining)
              rest-matches (rest remaining)]
          (if (empty? ranking-lines) 						
            (recur rest-matches [[current-match]] 0)	
            (let [ranking-line (ranking-lines index)
                  last-match (peek ranking-line)
                  is-series (match-series? player last-match current-match)]
              (if is-series
                (recur rest-matches 
                       (assoc-in ranking-lines [index] 
                                 (conj ranking-line current-match))
                       0)
                (if (> (count ranking-lines) (+ index 1)) 
                  (recur remaining ranking-lines (+ index 1))
                  (recur rest-matches (conj ranking-lines [current-match]) 0) )))))
        ranking-lines))))

(defn- get-subsequent-matches [player last-match lines]
  (first (for [line lines
               :let [first-match (first line)] 
               :when (match-series? player last-match first-match)]
           line)))

(defn- merge-lines-if-necessary [player matches ranking-lines]
  (loop [lines ranking-lines 
         result-lines []]
    (if-let [ranking-lines (seq lines)]
      (let [current-line (first ranking-lines)
            last-of-current-line (peek current-line)
            rest-lines (rest ranking-lines) 
            subsequent-matches (get-subsequent-matches player last-of-current-line rest-lines)]
        (recur 
          (filter #(not= % subsequent-matches) rest-lines)
          (conj result-lines (into current-line subsequent-matches))))
      result-lines)))

(defn- get-matches-lines [player matches]
  (let [ranking-lines (compute-ranking-lines player matches)
        merged-lines (merge-lines-if-necessary player matches ranking-lines)]
    (sort-by #((comp - count) %) merged-lines)))

(defn- get-matches-line [player lines]
  (if (= (count lines) 0) []
    (condp = player
      momsen (into (nth lines 0) (nth lines 1))
      pro (into (nth lines 0) (nth lines 1))
      (nth lines 0))))

(defn- get-matches-with-avg-maxbreak [player matches]
  (let [cleaned-matches (filter #(not= (max-break player %) 0) (sorted-matches-of player matches))]
    (loop [remaining-matches cleaned-matches
           processed-matches []
           max-break-line []]
      (if (seq remaining-matches)
        (let [current-match (first remaining-matches)
              divisor (-> (count processed-matches) inc)
              avg-max-break (/ (reduce #(+ % (max-break player %2)) 0 (conj processed-matches current-match)) divisor)]
          (recur (rest remaining-matches)
                 (conj processed-matches current-match)
                 (conj max-break-line (assoc current-match :avg-max-break avg-max-break))))
        max-break-line))))

(defn- adjust-date [match]
  (assoc match :date (timef/parse date-formatter (:date match))))

;; (defn show-ranking-line [players matches]
;; 	(let [first-player (first players)
;; 		  first-player-line (get-matches-line first-player (get-matches-lines first-player matches))
;; 		  plot (time-series-plot (map #(coerce/to-long (:date %)) first-player-line) (map (partial ranking first-player) first-player-line) :legend true :series-label first-player)]
;; 		(doseq [player (rest players)]
;; 			(let [line (get-matches-line player (get-matches-lines player matches))]
;; 				(add-lines plot (map #(coerce/to-long (:date %)) line) (map (partial ranking player) line) :series-label player)))
;; 		(view plot)))

;; (defn get-matches-with-avg-maxbreak [player matches]
;; 	(let [cleaned-matches (filter #(not= (max-break player %) 0) (sorted-matches-of player matches))]
;; 		(loop [remaining-matches cleaned-matches
;; 			   processed-matches []
;; 			   max-break-line []]
;; 			(if (seq remaining-matches)
;; 				(let [current-match (first remaining-matches)
;; 					  divisor (-> (count processed-matches) inc)
;; 					  avg-max-break (/ (reduce #(+ % (max-break player %2)) 0 (conj processed-matches current-match)) divisor)]
;; 					(recur (rest remaining-matches)
;; 						   (conj processed-matches current-match)
;; 						   (conj max-break-line (assoc current-match :avg-max-break avg-max-break))))
;; 				max-break-line))))

;; (defn show-maxbreak-line [players matches]
;; 	(let [first-player (first players)
;; 		  first-player-matches (sorted-matches-of first-player matches)
;; 		  first-player-line (get-matches-line first-player (get-matches-lines first-player first-player-matches))
;; 		  first-player-maxbreak-line (get-matches-with-avg-maxbreak first-player (filter #(not= 0 (max-break first-player %)) first-player-line))
;; 		  plot (time-series-plot (map #(coerce/to-long (:date %)) first-player-maxbreak-line) (map #(:avg-max-break %) first-player-maxbreak-line) :legend true :series-label first-player)]
;; 		(doseq [player (rest players)]
;; 			(let [player-matches (sorted-matches-of player matches)
;; 				  line (get-matches-with-avg-maxbreak player (filter #(not= 0 (max-break player %)) (get-matches-line player (get-matches-lines player player-matches))))]
;; 				(add-lines plot (map #(coerce/to-long (:date %)) line) (map #(:avg-max-break %) line) :series-label player)))
;; 		(view plot)))

;; (defn all-100-matches-series [player matches]
;;   (let [player-matches (sorted-matches-of player matches)
;;         player-line (get-matches-line player (get-matches-lines player player-matches))
;;         matches-nr (count player-line)]
;;     (loop [remaining-matches player-line
;;            complete-series []]
;;       (if (and (<= 100 (count remaining-matches)) (seq remaining-matches))
;;         (recur (rest remaining-matches) (conj complete-series (take 100 remaining-matches)))
;;         complete-series)
;;       )
;;     ))

;; (defn series-with-best-avg-maxbreak [player matches]
;;   (let [serieses (all-100-matches-series player matches)
;;         cleaned-serieses (map (fn [series] (filter (fn [match] (not= (max-break player match) 0)) series)) serieses)]
;;     23))










