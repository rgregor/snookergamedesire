package com.greg.gamedesire.server;

public interface IMatchesFilter {
	String filterMatches(String jsonMatches, int dropMatches, String[] players);
}
