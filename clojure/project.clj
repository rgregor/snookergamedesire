(defproject cljdesire "1.0.0-SNAPSHOT"
  :description "FIXME: write description"
  :source-paths ["src/main/clojure/"]
  :aot [com.greg.gamedesire.server.impl.core]
  :java-source-paths ["src/main/java"]
  :dependencies [[org.clojure/clojure "1.4.0"]
  				[org.clojure/data.json "0.1.3"]
  				[clj-time "0.4.4"]
  				[incanter/incanter "1.3.0" :exclusions [incanter/incanter-mongodb]]])
